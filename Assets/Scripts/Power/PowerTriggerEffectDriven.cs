﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Traps
{
    public class PowerTriggerEffectDriven : MonoBehaviour, IEffectInfluence
    {
        [SerializeField, ReorderableList] PowerActivatable[] linkedActivatables;
        [SerializeField, OnValueChanged("SetTriggerVisuals")] ActivationEffect activationEffect;

        [Header("Rotating Object")]
        [SerializeField] float rotationSpeed = 5;
        [SerializeField] Transform rotator;
        [SerializeField] GameObject windBlades;
        [SerializeField] GameObject waterBlades;

        [Space, SerializeField] bool debugPowerActive;

        enum ActivationEffect { WindOrWater, Wind, Water }
        HashSet<EffectVolumeBase> interactingEffects = new HashSet<EffectVolumeBase>();

        bool isActive;
        bool requiredEffectActive;

        void Start()
        {
            SetTriggerVisuals();
        }

        void FixedUpdate()
        {
            if (debugPowerActive) SetTrapsActive(true);
            else SetTrapsActive(requiredEffectActive);

            // rotate object if active
            if (isActive) rotator.Rotate(0, 0, rotationSpeed);
        }

        void OnDrawGizmosSelected()
        {
            // draw lines between trigger and activatables
            Gizmos.color = Color.cyan;
            foreach (PowerActivatable a in linkedActivatables) if (a) Gizmos.DrawLine(transform.position, a.transform.position);
        }

        public void OnEffectUpdated(EffectVolumeBase effectVolume)
        {
            if (effectVolume.IsActive) interactingEffects.Add(effectVolume);
            else interactingEffects.Remove(effectVolume);
            requiredEffectActive = CheckForRequiredEffects();
        }

        bool CheckForRequiredEffects()
        {
            foreach (EffectVolumeBase ev in interactingEffects)
            {
                switch (activationEffect)
                {
                    case ActivationEffect.WindOrWater: if ((ev.ActiveEffect.IsWind() || ev.ActiveEffect.IsWater())) return true; break;

                    case ActivationEffect.Wind: if (ev.ActiveEffect.IsWind()) return true; break;

                    case ActivationEffect.Water: if (ev.ActiveEffect.IsWater()) return true; break;
                }
            }
            return false;
        }

        void SetTrapsActive(bool active)
        {
            if (isActive != active)
            {
                isActive = active;
                foreach (PowerActivatable pa in linkedActivatables) if (pa) pa.SetPower(active);
            }
        }

        void SetTriggerVisuals()
        {
            windBlades.SetActive(false);
            waterBlades.SetActive(false);

            if (activationEffect == ActivationEffect.WindOrWater)
            {
                windBlades.SetActive(true);
                waterBlades.SetActive(true);
                return;
            }
            if (activationEffect == ActivationEffect.Wind)
            {
                windBlades.SetActive(true);
                return;
            }
            waterBlades.SetActive(true);
        }
    }
}
