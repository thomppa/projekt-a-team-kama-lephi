﻿using System;
using UnityEngine;
using NaughtyAttributes;

public class PowerActivatable : MonoBehaviour
{
    [SerializeField, OnValueChanged("OnPowerSet")] bool debugPowerActive;

    public bool PowerActive { get => powerActive || debugPowerActive; }

    bool powerActive = false;

    public void SetPower(bool active)
    {
        powerActive = active;
        OnPowerSet(active);
    }

    protected virtual void OnPowerSet(bool powerActive) { }
}
