﻿using System;
using System.Collections;
using System.Collections.Generic;
using Traps;
using UnityEngine;
using Variante_4;

public class TrapFirePattern: MonoBehaviour
{
    private PowerActivatable _myTrap;

    [SerializeField] private float maxAngle;
    private float angle = 0f;
    private float coherentAngle;

    // [SerializeField] [RangeEx(0, 1000, 100)]
    // private float fireIntervalInMilliseconds;

    [SerializeField] [Range(0, 3000)] private float fireIntervalInMilliseconds;

    private Vector2 BulletMoveDirection;
    private int _additionalStreamCount;
    private float _relativeAngle;
    private float _coherentRelativeAngle;
    [SerializeField] private float _angleSteps;


    public enum ShotStreams
    {
        OneStream,
        TwoStreams,
        ThreeStreams,
        FourStreams,
    }
    public ShotStreams ShotPattern;

    public enum CoherentStream
    {
        Off,
        On,
    }

    public CoherentStream coherentStream;

    [SerializeField] private float _coherentAngleSteps;

    public enum ChangeDirection
    {
        Off,
        On,
    }
    public ChangeDirection directionChangeMode;

    private float _fireTimer = 0;
    private float _directionChangeTimer = 0;
    [SerializeField] private float changeStartTime = 5f;
    [SerializeField] private float changeInterval = 5f;

    private bool _directionChanged = false;


    private void Awake()
    {
        _myTrap = GetComponent<PowerActivatable>();

        switch (ShotPattern)
        {
            case ShotStreams.OneStream:
                _additionalStreamCount = 0;
                _relativeAngle = 360f;
                _coherentRelativeAngle = -360f;
                break;
            case ShotStreams.TwoStreams:
                _additionalStreamCount = 1;
                _coherentRelativeAngle = -180f;
                break;
            case ShotStreams.ThreeStreams:
                _additionalStreamCount = 2;
                _relativeAngle = 120f;
                _coherentRelativeAngle = -120f;
                break;
            case ShotStreams.FourStreams:
                _additionalStreamCount = 3;
                _relativeAngle = 90;
                _coherentRelativeAngle = -90;
                break;
            default:
                _additionalStreamCount = 0;
                _relativeAngle = 360f;
                _coherentRelativeAngle = -360f;
                break;
        }
    }

    private void DirectionChange()
    {
        if (!_directionChanged)
        {
            _directionChanged = true;
            _angleSteps = -(Mathf.Abs(_angleSteps));
            _coherentAngleSteps = -Mathf.Abs(_coherentAngleSteps);
        }
        else
        {
            _directionChanged = false;
            _angleSteps = Mathf.Abs(_angleSteps);
            _coherentAngleSteps = Mathf.Abs(_coherentAngleSteps);
        }
    }

    private void Update()
    {
        if (_myTrap.PowerActive)
        {
            _fireTimer += Time.deltaTime;
            _directionChangeTimer += Time.deltaTime;

            if (_fireTimer >= fireIntervalInMilliseconds / 1000)
            {
                _fireTimer = 0;

                switch (coherentStream)
                {
                    case CoherentStream.Off:
                        Fire();
                        break;
                    case CoherentStream.On:
                        Fire();
                        FireCoherentStreams();
                        break;
                }
            }

            if (_directionChangeTimer >= changeInterval)
            {
                _directionChangeTimer = 0;

                switch (directionChangeMode)
                {
                    case ChangeDirection.Off:
                        break;
                    case ChangeDirection.On:
                        DirectionChange();
                        break;
                }
            }
        }
    }

    private void Fire()
    {
        for (int i = 0; i <= _additionalStreamCount; i++)
        {
            float bulDirX = transform.position.x + Mathf.Sin(((angle + _relativeAngle * i) * Mathf.PI) / 180f);
            float bulDirY = transform.position.y + Mathf.Cos(((angle + _relativeAngle * i) * Mathf.PI) / 180f);

            Vector3 bulMoveVec = new Vector3(bulDirX, bulDirY, 0f);
            Vector3 buldir = (bulMoveVec - transform.position).normalized;

            GameObject bul = ProjectilePool.Instance.GetProjectile();
            bul.transform.position = transform.position;
            bul.transform.rotation = transform.rotation;
            bul.SetActive(true);
            bul.GetComponent<TrapProjectile>().SetMoveDirection(buldir);
        }

        angle += _angleSteps;

        if (angle >= maxAngle)
        {
            angle = 0f;
        }
    }

    private void FireCoherentStreams()
    {
        for (int i = 0; i <= _additionalStreamCount; i++)
        {
            float bulDirX = transform.position.x + Mathf.Sin(((coherentAngle + _coherentRelativeAngle * i) * Mathf.PI) / 180f);
            float bulDirY = transform.position.y + Mathf.Cos(((coherentAngle + _coherentRelativeAngle * i) * Mathf.PI) / 180f);

            Vector3 bulMoveVec = new Vector3(bulDirX, bulDirY, 0f);
            Vector3 buldir = (bulMoveVec - transform.position).normalized;

            GameObject bul = ProjectilePool.Instance.GetProjectile();
            bul.transform.position = transform.position;
            bul.transform.rotation = transform.rotation;
            bul.SetActive(true);
            bul.GetComponent<TrapProjectile>().SetMoveDirection(buldir);

        }
        coherentAngle -= _coherentAngleSteps;

        if (angle <= -maxAngle)
        {
            angle = 0f;
        }
    }
}
