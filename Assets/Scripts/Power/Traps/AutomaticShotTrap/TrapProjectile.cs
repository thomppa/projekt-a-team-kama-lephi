﻿using UnityEngine;

//Contributors: Kay
namespace Variante_4
{
    public class TrapProjectile : MonoBehaviour
    {
        [SerializeField] float projectileSpeed;

        public Vector2 direction;
        public LayerMask collisionLayers;

        private void Update()
        {
            transform.Translate(direction * projectileSpeed * Time.deltaTime);
        }

        public void SetMoveDirection(Vector3 dir)
        {
            direction = dir;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (other.TryGetComponent(out PlayerHealthSystem health)) health.Damage(20);
                DestroyProjectile();
            }
            if (collisionLayers == (collisionLayers | (1 << other.gameObject.layer))) DestroyProjectile();
        }

        private void OnBecameInvisible()
        {
            DestroyProjectile();
        }

        private void DestroyProjectile()
        {
            gameObject.SetActive(false);
        }
    }
}
