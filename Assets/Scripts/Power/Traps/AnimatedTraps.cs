﻿using System;
using UnityEngine;

namespace Traps
{
    public class AnimatedTraps : PowerActivatable
    {
        private Animator[] _myAnim;
        private static readonly int TrapActivated = Animator.StringToHash("TrapActivated");
        private static readonly int TrapDeactivated = Animator.StringToHash("TrapDeactivated");

        protected void Awake()
        {
            _myAnim = GetComponentsInChildren<Animator>();
        }

        protected void FixedUpdate()
        {
            if (PowerActive)
            {
                foreach (var trap in _myAnim)
                {
                    trap.SetTrigger(TrapActivated);
                }
            }
            else
            {
                foreach (var trap in _myAnim)
                {
                    trap.SetTrigger(TrapDeactivated);
                }
            }

        }
    }
}
