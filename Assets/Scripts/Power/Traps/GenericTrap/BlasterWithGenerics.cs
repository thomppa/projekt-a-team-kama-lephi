﻿using System;
using Traps;
using UnityEngine;
using UnityEngine.UI;

namespace Variante2
{
    public class BlasterWithGenerics : MonoBehaviour
    {
        private PowerActivatable _myTrap;
        [SerializeField] private float refireRate = 2;

        private float _fireTimer;

        public enum BulletRepeat
        {
            NoRepetition,
            Double,
            Triple,
            Quadruple,
            IrgendwasMitFünf,
        }

        public BulletRepeat repetitionPattern;

        [SerializeField] [Range(0, 1)] private float repetitionDelay;

        private void Awake()
        {
            _myTrap = GetComponentInParent<PowerActivatable>();
        }

        void Update()
        {
            if (_myTrap.PowerActive)
            {
                _fireTimer += Time.deltaTime; //simpler Timer
                if (_fireTimer >= refireRate) //wenn RefireTimer erreicht
                {
                    _fireTimer = 0; //Timer wieder 0

                    switch (repetitionPattern)
                    {
                        case BulletRepeat.NoRepetition:
                            Fire();
                            break;
                        case BulletRepeat.Double:
                            Invoke(nameof(Fire), 0f);
                            Invoke(nameof(Fire), repetitionDelay);
                            break;
                        case BulletRepeat.Triple:
                            Invoke(nameof(Fire), 0f);
                            Invoke(nameof(Fire), repetitionDelay);
                            Invoke(nameof(Fire), repetitionDelay * 2);
                            break;
                        case BulletRepeat.Quadruple:
                            Invoke(nameof(Fire), 0f);
                            Invoke(nameof(Fire), repetitionDelay);
                            Invoke(nameof(Fire), repetitionDelay * 2);
                            Invoke(nameof(Fire), repetitionDelay * 3);
                            break;
                        case BulletRepeat.IrgendwasMitFünf:
                            Invoke(nameof(Fire), 0f);
                            Invoke(nameof(Fire), repetitionDelay);
                            Invoke(nameof(Fire), repetitionDelay * 2);
                            Invoke(nameof(Fire), repetitionDelay * 3);
                            Invoke(nameof(Fire), repetitionDelay * 4);
                            break;
                    }
                }
            }
        }

        private void Fire()
        {
            var shot = ShotPool.Instance.Get();
            shot.transform.rotation = transform.rotation;
            shot.transform.position = transform.position;
            shot.gameObject.SetActive(true);
        }
    }
}
