﻿using System;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;

//Contributors: Kay
namespace Traps
{
    public class HomingProjectile : MonoBehaviour
    {
        [SerializeField] private float projectileSpeed;

        public Vector3 direction;
        void Start()
        {
            direction = PlayerController.Instance.transform.position - transform.position;
            direction.Normalize();
        }

        private void Update()
        {
            transform.position += direction * (projectileSpeed * Time.deltaTime);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                PlayerController.Instance.GetComponent<PlayerHealthSystem>().Kill(); ;
                DestroyProjectile();
            }
        }

        private void OnEnable()
        {
            Invoke(nameof(DestroyProjectile), 3f);
        }

        private void OnBecameInvisible()
        {
            DestroyProjectile();
        }

        private void DestroyProjectile()
        {
            gameObject.SetActive(false);
        }
    }
}
