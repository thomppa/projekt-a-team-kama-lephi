﻿using System;
using UnityEngine;
using NaughtyAttributes;

//CONTRIBUTORS: Kay
namespace Traps
{
    public class PowerTriggerVolume : MonoBehaviour
    {
        [SerializeField] bool stayActive;
        [SerializeField, ReorderableList] PowerActivatable[] linkedActivatables;

        bool isActive;

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player")) SetTrapsActive(true);
        }

        void OnTriggerExit(Collider other)
        {
            if (!stayActive && other.CompareTag("Player")) SetTrapsActive(false);
        }

        void OnDrawGizmosSelected()
        {
            // draw lines between trigger and activatables
            Gizmos.color = Color.cyan;
            foreach (PowerActivatable a in linkedActivatables) if (a) Gizmos.DrawLine(transform.position, a.transform.position);
        }

        void SetTrapsActive(bool active)
        {
            if (isActive != active)
            {
                isActive = active;
                foreach (PowerActivatable pa in linkedActivatables) pa.SetPower(active);
            }
        }
    }
}
