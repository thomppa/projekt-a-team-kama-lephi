﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(DamageVolume))]
public class EffectVolumeWater : EffectVolumeBase
{
    [SerializeField, Foldout("Advanced Properties")] MeshRenderer waterBodyRenderer;
    [SerializeField, Foldout("Advanced Properties")] GameObject waterCurrent;
    [SerializeField, Foldout("Advanced Properties")] float fillSpeed = 0.1f;
    [SerializeField, Foldout("Advanced Properties")] AudioSource activationAudio;

    bool isWaterCurrent;
    Vector3 originalScale;

    public override Effect ActiveEffect { get => isWaterCurrent ? Effect.WaterCurrent : Effect.Water; }

    protected override void Start()
    {
        Vector3 scale = waterCurrent.transform.localScale;
        waterCurrent.transform.localScale = new Vector3(scale.x, scale.y, (1 / waterCurrent.transform.lossyScale.z) * 5);
        waterCurrent.SetActive(false);

        originalScale = transform.localScale;
        GetComponent<DamageVolume>().IsActive = false;
        base.Start();
    }

    public override void ShowPreview(bool show)
    {
        if (isActive && show) show = false;
        base.ShowPreview(show);
    }

    protected override void EvaluateInteractionsForEffectChange(bool forceNotification = false)
    {
        bool interactingWithWind = false;

        foreach (EffectVolumeBase ev in interactingEffects)
        {
            if (ev.ActiveEffect.IsWind())
            {
                interactingWithWind = true;
                break;
            }
        }

        if (interactingWithWind && !isWaterCurrent)
        {
            ActivateWaterCurrent();
            NotifyAllInteractions(isActive);
            return;
        }

        if (!interactingWithWind && isWaterCurrent)
        {
            DeactivateWaterCurrent();
            NotifyAllInteractions(isActive);
            return;
        }

        if (forceNotification)
        {
            NotifyAllInteractions(isActive);
            return;
        }
    }

    public override void Activate()
    {
        //isWaterCurrent = false;
        base.Activate();
        activationAudio?.Play();
        StartCoroutine(FillWater(true, 0.1f, originalScale.y));
    }

    public override void Deactivate()
    {
        activationAudio?.Play();
        isWaterCurrent = false;
        waterCurrent.SetActive(false);
        waterBodyRenderer.material.SetInt("isWaterCurrent", 0);
        GetComponent<DamageVolume>().IsActive = false;
        base.Deactivate();
        StartCoroutine(FillWater(false, originalScale.y, 0.1f));
    }

    void ActivateWaterCurrent()
    {
        isWaterCurrent = true;
        waterCurrent.SetActive(true);
        waterBodyRenderer.material.SetInt("isWaterCurrent", 1);
        GetComponent<DamageVolume>().IsActive = true;
    }

    void DeactivateWaterCurrent()
    {
        isWaterCurrent = false;
        waterCurrent.SetActive(false);
        waterBodyRenderer.material.SetInt("isWaterCurrent", 0);
        GetComponent<DamageVolume>().IsActive = false;
    }

    IEnumerator FillWater(bool showAfter, float heightStart, float heightEnd)
    {
        transform.localScale = new Vector3(originalScale.x, heightStart, originalScale.z);
        visualsEffect.SetActive(true);
        float t = 0;

        while (transform.localScale.y != heightEnd)
        {
            transform.localScale = new Vector3(originalScale.x, Mathf.Lerp(heightStart, heightEnd, t * fillSpeed), originalScale.z);
            t += Time.deltaTime;
            yield return null;
        }
        if (!showAfter) visualsEffect.SetActive(false);
    }
}
