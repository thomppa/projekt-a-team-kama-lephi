﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEffectInfluence 
{
    /// <summary>
    /// Is called when an interacting EffectVolume is updated.
    /// </summary>
    /// <param name="effectVolume">Interacting EffectVolume that was updated.</param>
    void OnEffectUpdated(EffectVolumeBase effectVolume);
}
