﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Module : MonoBehaviour
{
    [SerializeField] bool damageOnActivation = true;
    [SerializeField, ShowIf("damageOnActivation")] float activationDamage = 5;
    [Space]
    [SerializeField] bool usableOnce;
    [Space, SerializeField] bool activeForTime;
    [SerializeField, ShowIf("activeForTime")] float timeActive = 5;
    [Space]
    [SerializeField] bool intervalToggle;
    [SerializeField, ShowIf("intervalToggle")] bool intertervalInteractable;
    [SerializeField, ShowIf("intervalToggle")] float activeTime = 5;
    [SerializeField, ShowIf("intervalToggle")] float inactiveTime = 5;

    [ReadOnly, SerializeField, ShowIf("intervalToggle")] bool intervalToggleActive;

    [SerializeField, ReorderableList] EffectVolumeBase[] controlledEffects;
    [SerializeField, ReorderableList] PowerActivatable[] linkedActivatables;

    [SerializeField, Foldout("Advanced Properties")] GameObject lightOn;
    [SerializeField, Foldout("Advanced Properties")] GameObject lightOff;
    [SerializeField, Foldout("Advanced Properties")] Material lampOn;
    [SerializeField, Foldout("Advanced Properties")] Material lampOff;
    [SerializeField, Foldout("Advanced Properties")] SkinnedMeshRenderer lampMesh;
    [SerializeField, Foldout("Advanced Properties")] Animator animator;

    bool isPowerActive;

    bool usedOnce;
    bool timerActive;

    bool isOnInterval;
    bool duringInterval;

    public EffectVolumeBase[] ControlledEffects { get => controlledEffects; }

    private void Start()
    {
        intervalToggleActive = intervalToggle;
        SetVisualActive(false);
    }

    private void Update()
    {
        if (intervalToggleActive && !duringInterval) StartCoroutine(IntervalToggle());
    }

    void OnDrawGizmosSelected()
    {
        // draw lines between module and controlled effects
        Gizmos.color = Color.yellow;
        foreach (EffectVolumeBase ev in controlledEffects) if (ev) Gizmos.DrawLine(transform.position, ev.transform.position);

        Gizmos.color = Color.cyan;
        foreach (PowerActivatable a in linkedActivatables) if (a) Gizmos.DrawLine(transform.position, a.transform.position);
    }

    [Button("Force Interaction", EButtonEnableMode.Playmode)]
    public void Interact()
    {
        if (usableOnce && usedOnce) return;
        if (intervalToggle && !intertervalInteractable) return;

        usedOnce = true;

        if (intervalToggle)
        {
            intervalToggleActive = !intervalToggleActive;
            if (!intervalToggleActive)
            {
                StopCoroutine(IntervalToggle());
                duringInterval = false;
            }
            return;
        }

        if (PlayerController.Instance.TryGetComponent(out PlayerHealthSystem health)) health.Damage(activationDamage, false, true);
        ToggleControlled();

        if (activeForTime && !intervalToggle)
            if (timerActive) StopCoroutine(ModuleTimer());
            else StartCoroutine(ModuleTimer());
    }

    void ToggleControlled()
    {
        foreach (EffectVolumeBase ev in controlledEffects) if (ev) ev.Toggle();

        isPowerActive = !isPowerActive;
        foreach (PowerActivatable pa in linkedActivatables) pa?.SetPower(isPowerActive);

        SetVisualActive(isPowerActive);
    }

    void SetVisualActive(bool isActive)
    {
        lampMesh.material = isActive ? lampOn : lampOff;
        lightOn.SetActive(isActive);
        lightOff.SetActive(!isActive);
        animator.speed = isActive ? 1 : 0;
    }

    IEnumerator ModuleTimer()
    {
        timerActive = true;
        yield return new WaitForSeconds(timeActive);
        ToggleControlled();
        timerActive = false;
    }

    IEnumerator IntervalToggle()
    {
        duringInterval = true;
        isOnInterval = !isOnInterval;
        ToggleControlled();
        yield return new WaitForSeconds(isOnInterval ? activeTime : inactiveTime);
        duringInterval = false;
    }
}
