﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using NaughtyAttributes;

[RequireComponent(typeof(DamageVolume))]
public class EffectVolumeFireMetal : EffectVolumeBase
{
    [SerializeField, Foldout("Advanced Properties")] float fillSpeed = 10;
    [SerializeField, Foldout("Advanced Properties")] VisualEffectAsset bubblesVFX;


    public override Effect ActiveEffect { get => Effect.Fire; }

    protected override void EvaluateInteractionsForEffectChange(bool forceNotification = false)
    {
        bool interactingWithWater = false;

        EffectVolumeBase waterEffect = null;

        foreach (EffectVolumeBase ev in interactingEffects)
            if (ev.ActiveEffect.IsWater())
            {
                interactingWithWater = true;
                waterEffect = ev;
                break;
            }

        if (interactingWithWater)
        {
            ActivatedUnderwater(waterEffect);
            Deactivate();
            return;
        }

        if (forceNotification)
        {
            NotifyAllInteractions(isActive);
            DestroyDestructibles();
            return;
        }
    }

    void DestroyDestructibles()
    {
        foreach (Collider c in GetCollidersTouching(influenceLayers))
            if (c.TryGetComponent(out Destructible d)) d.Destruct(true);
    }

    public override void Activate()
    {
        base.Activate();
        StartCoroutine(IncreaseMetal());
    }

    public override void Deactivate()
    {
        base.Deactivate();
        StartCoroutine(DecreaseMetal());
    }

    void ActivatedUnderwater(EffectVolumeBase water)
    {
        AudioManager.instance.PlaySound("FireUnderwater");
        VisualEffect vfx = VFXHelper.SpawnVFX(transform.position, bubblesVFX, 10);
        vfx.SetFloat("WaterSurfacePositionY", water.transform.position.y + water.Height + 0.3f);
        vfx.SetFloat("SpawnWidth", Width);
    }

    IEnumerator IncreaseMetal()
    {
        visualsEffect.transform.localScale = new Vector3(1, 0, 1);
        visualsEffect.transform.localPosition = Vector3.zero;
        float t = 0;

        while (visualsEffect.transform.localScale.y != 1)
        {
            visualsEffect.transform.localScale = new Vector3(1, Mathf.Lerp(0, 1, t * fillSpeed), 1);
            t += Time.deltaTime;
            yield return null;
        }

        visualsEffect.transform.localScale = new Vector3(1, 1, 1);
    }

    IEnumerator DecreaseMetal()
    {
        visualsEffect.transform.localScale = new Vector3(1, 1, 1);
        visualsEffect.SetActive(true);
        float t = 0;

        while (visualsEffect.transform.localScale.y != 0)
        {
            float height = Mathf.Lerp(1, 0, t * fillSpeed);
            visualsEffect.transform.localScale = new Vector3(1, height, 1);
            visualsEffect.transform.localPosition = new Vector3(0, -(1 - height), 0);
            t += Time.deltaTime;
            yield return null;
        }

        visualsEffect.SetActive(false);
        visualsEffect.transform.localScale = new Vector3(1, 1, 1);
        visualsEffect.transform.localPosition = Vector3.zero;
    }
}
