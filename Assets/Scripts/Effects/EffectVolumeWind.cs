﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class EffectVolumeWind : EffectVolumeBase
{
    [SerializeField] bool isConveyorBelt = false;
    [SerializeField, OnValueChanged("UpdateWindDirection")] WindDirection windDirection = WindDirection.Up;

    public override WindDirection ActiveWindDirection { get => windDirection; }
    public override Effect ActiveEffect { get => Effect.Wind; }

    protected override void Start()
    {
        base.Start();
        UpdateWindDirection();
    }

    protected override void EvaluateInteractionsForEffectChange(bool forceNotification = false)
    {
        if (forceNotification) NotifyAllInteractions(isActive);
    }

    public override void Activate()
    {
        base.Activate();
        if (isConveyorBelt) visualsEffect.SetActive(false);
    }

    public override void ShowPreview(bool show)
    {
        if (isConveyorBelt) return;
        base.ShowPreview(show);
    }

    void UpdateWindDirection()
    {
        switch (windDirection)
        {
            case WindDirection.Left:
                visualsPreview.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));
                visualsEffect.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));
                break;
            case WindDirection.Right:
                visualsPreview.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
                visualsEffect.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -90));
                break;
            default:
                visualsPreview.gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                visualsEffect.gameObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                break;
        }
    }
}
