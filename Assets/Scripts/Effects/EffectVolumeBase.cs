﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(BoxCollider))]
public abstract class EffectVolumeBase : MonoBehaviour
{
    [SerializeField] protected bool startActive;
    [SerializeField, Foldout("Advanced Properties")] protected LayerMask influenceLayers;
    [SerializeField, Foldout("Advanced Properties")] protected GameObject visualsPreview;
    [SerializeField, Foldout("Advanced Properties")] protected GameObject visualsEffect;
    [SerializeField, Foldout("Advanced Properties")] protected AudioSource audioSource;

    protected bool isActive;
    protected bool isShowingPreview;
    protected bool isMoving;
    protected HashSet<EffectVolumeBase> interactingEffects = new HashSet<EffectVolumeBase>();
    protected BoxCollider boxCollider;
    protected BoxParameter defaultBox;

    Vector3 lastPosition;

    public struct BoxParameter
    {
        public Vector3 center, size;
        public BoxParameter(Vector3 center, Vector3 size)
        {
            this.center = center;
            this.size = size;
        }
    }

    public float Height { get => transform.lossyScale.y * boxCollider.size.y; }
    public float Width { get => transform.lossyScale.x * boxCollider.size.x; }
    public bool IsActive { get => isActive; }
    public abstract Effect ActiveEffect { get; }
    public virtual WindDirection ActiveWindDirection { get => WindDirection.Up; }

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        defaultBox = new BoxParameter(boxCollider.center, boxCollider.size);
        boxCollider.enabled = false;

        lastPosition = transform.position;
        InitializeVisuals();

        if (startActive) Activate();
    }

    protected virtual void FixedUpdate()
    {
        if (isActive && lastPosition != transform.position)
        {
            CheckForInteractions();
            isMoving = true;
        }
        else isMoving = false;
        lastPosition = transform.position;
    }

    public virtual void Activate()
    {
        isActive = true;
        boxCollider.enabled = true;
        visualsEffect.SetActive(true);
        if (audioSource) audioSource.Play();

        if (isShowingPreview) ShowPreview(false);
        CheckForInteractions();
    }

    public virtual void Deactivate()
    {
        isActive = false;
        visualsEffect.SetActive(false);
        if (audioSource) audioSource.Stop();

        NotifyAllInteractions(false);
        boxCollider.enabled = false;
        interactingEffects.Clear();
    }

    protected virtual void InitializeVisuals()
    {
        visualsEffect.SetActive(false);
        visualsPreview.SetActive(false);
    }

    protected abstract void EvaluateInteractionsForEffectChange(bool forceNotification = false);

    public virtual void ShowPreview(bool show)
    {
        if (show != isShowingPreview)
        {
            isShowingPreview = show;
            visualsPreview.SetActive(show);
        }
    }

    /// <summary>
    /// Activates or deacivates the effect depending on the prior state.
    /// </summary>
    [Button("Toggle Effect", EButtonEnableMode.Playmode)]
    public void Toggle()
    {
        if (isActive) Deactivate();
        else Activate();
    }

    /// <summary>
    /// Notify this effect about the interaction with another EffectVolume.
    /// </summary>
    public void InteractionNotification(EffectVolumeBase effectVolume, bool effectIsActive)
    {
        if (isActive && effectVolume != this)
        {
            if (effectIsActive) interactingEffects.Add(effectVolume);
            else interactingEffects.Remove(effectVolume);
            EvaluateInteractionsForEffectChange();
        }
    }

    protected void CheckForInteractions()
    {
        List<EffectVolumeBase> previousInteractingEffects = interactingEffects.ToList();

        interactingEffects.Clear();
        foreach (Collider c in GetCollidersTouching(1 << LayerMask.NameToLayer("Effect")))
            if (c.TryGetComponent(out EffectVolumeBase ev) && ev.IsActive) interactingEffects.Add(ev);

        if (isMoving)
        {
            foreach (EffectVolumeBase ev in interactingEffects.ToList()) previousInteractingEffects.Remove(ev);
            foreach (EffectVolumeBase ev in previousInteractingEffects) ev.InteractionNotification(this, false);
        }

        EvaluateInteractionsForEffectChange(true);
    }

    protected void NotifyAllInteractions(bool effectIsActive)
    {
        foreach (EffectVolumeBase ev in interactingEffects.ToList()) ev.InteractionNotification(this, effectIsActive);
    }

    protected Vector3 BoxCenterToWorld(Vector3 center)
    {
        return transform.TransformPoint(center);
    }

    protected Vector3 BoxSizeToWorld(Vector3 size)
    {
        Vector3 worldSize = transform.TransformVector(size);
        return new Vector3(Mathf.Abs(worldSize.x), Mathf.Abs(worldSize.y), Mathf.Abs(worldSize.z));
    }

    protected Collider[] GetCollidersTouching(LayerMask layers)
    {
        return Physics.OverlapBox(BoxCenterToWorld(boxCollider.center), BoxSizeToWorld(boxCollider.size) / 2, Quaternion.identity, layers, QueryTriggerInteraction.Collide);
    }
}
