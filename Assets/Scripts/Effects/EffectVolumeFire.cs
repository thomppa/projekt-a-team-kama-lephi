﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using NaughtyAttributes;

[RequireComponent(typeof(DamageVolume))]
public class EffectVolumeFire : EffectVolumeBase
{
    [SerializeField, Range(0, 20)] float largeFireHeight = 5;
    [SerializeField, Foldout("Advanced Properties")] VisualEffectAsset bubblesVFX;
    [SerializeField, Foldout("Advanced Properties")] VisualEffect previewFireVFX;
    [SerializeField, Foldout("Advanced Properties")] VisualEffect fireVFX;
    [SerializeField, Foldout("Advanced Properties"), Range(0.5f, 10)] float fireVFXRate = 1;

    bool isLargeFire;

    public override Effect ActiveEffect { get => Effect.Fire; }

    protected override void Start()
    {
        base.Start();
        // adjust vfx scaling
        fireVFX.transform.localScale = new Vector3(0.5f, 1 / fireVFX.transform.lossyScale.y, 0.5f);
        previewFireVFX.transform.localScale = new Vector3(0.5f, 1 / previewFireVFX.transform.lossyScale.y, 0.5f);
        previewFireVFX.SetFloat("EffectHeight", Height);
        previewFireVFX.playRate = fireVFXRate;
    }

    protected override void EvaluateInteractionsForEffectChange(bool forceNotification = false)
    {
        bool interactingWithWind = false;
        bool interactingWithWater = false;

        EffectVolumeBase waterEffect = null;

        foreach (EffectVolumeBase ev in interactingEffects)
        {
            if (ev.ActiveEffect.IsWind()) interactingWithWind = true;

            if (ev.ActiveEffect.IsWater())
            {
                interactingWithWater = true;
                waterEffect = ev;
            }
        }

        if (interactingWithWater)
        {
            ActivatedUnderwater(waterEffect);
            Deactivate();
            return;
        }

        if (interactingWithWind && !isLargeFire)
        {
            ActivateLargeFire();
            return;
        }

        if (!interactingWithWind && isLargeFire)
        {
            DeactivateLargeFire();
            return;
        }

        if (forceNotification)
        {
            NotifyAllInteractions(isActive);
            DestroyDestructibles();
            return;
        }
    }

    void DestroyDestructibles()
    {
        foreach (Collider c in GetCollidersTouching(influenceLayers))
            if (c.TryGetComponent(out Destructible d)) d.Destruct(true);
    }

    public override void Activate()
    {
        UpdateFireVFX(true);
        base.Activate();
    }

    public override void Deactivate()
    {
        UpdateFireVFX(false);
        base.Deactivate();
    }

    void ActivateLargeFire()
    {
        isLargeFire = true;

        // enlarge effect and collider
        LocalLargeFireBox(defaultBox, out BoxParameter largeFire);
        boxCollider.center = largeFire.center;
        boxCollider.size = largeFire.size;
        visualsEffect.transform.localPosition = largeFire.center;
        visualsEffect.transform.localScale = largeFire.size;

        if (audioSource) audioSource.Play();
        UpdateFireVFX(true);
        CheckForInteractions();
        DestroyDestructibles();
    }

    void DeactivateLargeFire()
    {
        isLargeFire = false;
        // notify interactions within large fire that the effect is deactivated
        NotifyAllInteractions(false);

        // reset scale to default fire
        boxCollider.center = defaultBox.center;
        boxCollider.size = defaultBox.size;
        visualsEffect.transform.localPosition = defaultBox.center;
        visualsEffect.transform.localScale = defaultBox.size;

        UpdateFireVFX(true);
        // notify objects inside small fire again
        CheckForInteractions();
    }

    void ActivatedUnderwater(EffectVolumeBase water)
    {
        AudioManager.instance.PlaySound("FireUnderwater");
        VisualEffect vfx = VFXHelper.SpawnVFX(transform.position, bubblesVFX, 10);
        vfx.SetFloat("WaterSurfacePositionY", water.transform.position.y + water.Height + 0.3f);
        vfx.SetFloat("SpawnWidth", Width);
    }

    void UpdateFireVFX(bool active)
    {
        if (active)
        {
            fireVFX.Play();
            fireVFX.playRate = fireVFXRate;
            fireVFX.SetFloat("EffectHeight", Height);
            //fireVFX.SetFloat("EffectWidth", Width);
        }
        else fireVFX.Stop();
    }

    void LocalLargeFireBox(BoxParameter defaultFire, out BoxParameter largeFire)
    {
        float worldFireHeight = largeFireHeight / transform.localScale.y;
        largeFire.center = defaultFire.center + new Vector3(0, worldFireHeight / 2, 0);
        largeFire.size = defaultFire.size + new Vector3(0, worldFireHeight, 0);
    }

    void OnDrawGizmosSelected()
    {
        if (!Application.isPlaying)
        {
            BoxCollider box = GetComponent<BoxCollider>();
            LocalLargeFireBox(new BoxParameter(box.center, box.size), out BoxParameter largeFire);

            Gizmos.color = new Color(1, 0, 0, 0.2f);
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawCube(largeFire.center, largeFire.size);
        }
    }
}
