﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using NaughtyAttributes;

[RequireComponent(typeof(BoxCollider), typeof(DamageVolume))]
public class EffectVolume : MonoBehaviour
{
    [SerializeField, OnValueChanged("OnDefaultEffectChanged")] MainEffect defaultEffect;
    [SerializeField, Foldout("Advanced Properties")] GameObject prefabWindEffect;
    [SerializeField, Foldout("Advanced Properties")] GameObject prefabWaterEffect;
    [SerializeField, Foldout("Advanced Properties")] GameObject prefabFireEffect;

    enum MainEffect { WindUp = 1, WindLeft, WindRight, Water, Fire }

    [Button("Spawn updatet EffectVolume", EButtonEnableMode.Editor)]
    void SpawnEffectVolume()
    {
        GameObject prefab = prefabWindEffect;
        if (defaultEffect == MainEffect.Water) prefab = prefabWaterEffect;
        if (defaultEffect == MainEffect.Fire) prefab = prefabFireEffect;

        GameObject go = PrefabUtility.InstantiatePrefab(prefab, transform.parent) as GameObject;
        go.transform.position = transform.position - new Vector3(0, transform.localScale.y / 2, 0);
        go.transform.localScale = transform.localScale;
    }
}
#endif