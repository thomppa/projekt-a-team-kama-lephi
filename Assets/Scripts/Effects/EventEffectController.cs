﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class EventEffectController : MonoBehaviour
{
    [SerializeField, ReorderableList] EffectVolumeBase[] controlledEffects;

    public void ActivateEffect()
    {
        foreach (EffectVolumeBase ev in controlledEffects) if (ev) ev.Activate();
    }

    public void DeactivateEffect()
    {
        foreach (EffectVolumeBase ev in controlledEffects) if (ev) ev.Deactivate();
    }
}
