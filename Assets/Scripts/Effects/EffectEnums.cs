﻿public enum Effect { None, Wind, Water, WaterCurrent, Fire }
public enum WindDirection { Up, Left, Right }

public static class EffectExtensions
{
    public static bool IsWind(this Effect e) { return e == Effect.Wind; }
    public static bool IsWater(this Effect e) { return e == Effect.Water || e == Effect.WaterCurrent; }
    public static bool IsCurrent(this Effect e) { return e == Effect.WaterCurrent; }
    public static bool IsFire(this Effect e) { return e == Effect.Fire; }
    public static bool IsHostile(this Effect e) { return e == Effect.Fire || e == Effect.WaterCurrent; }
}
