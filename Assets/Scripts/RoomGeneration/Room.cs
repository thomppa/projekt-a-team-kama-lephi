﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(BoxCollider))]
public class Room : MonoBehaviour
{
    [SerializeField, OnValueChanged("SetRoomTriggerSize")] Vector2 roomSize = new Vector2(48, 27);
    [SerializeField] Transform respawnPoint;

    public Vector2 Size { get => roomSize; }

    void Awake()
    {
        SetRoomTriggerSize();
    }

    public bool TryGetRespawnPosition(out Vector3 respawnPosition)
    {
        if (respawnPoint)
        {
            respawnPosition = respawnPoint.position;
            return true;
        }
        else
        {
            respawnPosition = Vector3.zero;
            return false;
        }
    }

    public bool IsDoorInDirection(Vector2 direction)
    {
        if (TryGetComponent(out WallBuilder wb)) return wb.IsDoorInDirection(direction);
        return false;
    }

    void SetRoomTriggerSize()
    {
        BoxCollider box = GetComponent<BoxCollider>();
        box.center = new Vector3(0, 0, 5);
        box.size = new Vector3(roomSize.x - 1.5f, roomSize.y - 3, 10);
    }
}
