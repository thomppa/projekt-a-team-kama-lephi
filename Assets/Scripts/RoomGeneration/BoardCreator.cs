﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.ComponentModel;
using System.Linq;
using NaughtyAttributes;
using Random = UnityEngine.Random;

//CONTRIBUTORS: Kay
public class BoardCreator : MonoBehaviour
{
    [InfoBox("Höhen und Breite der Räume für den Generator. Bitte nur ändern, wenn sich auch alle Prefabs in den Maßen ändern!", EInfoBoxType.Warning)]
    [SerializeField] private float roomHeight = 9;
    [SerializeField] private float roomWidth = 16;
    
    #region Declaration of ShapeBoards

    [InfoBox("Das hier ist nur zum Debuggen im Inspector. Hier werden die absoluten Werte der Bitboards gezeigt, Wenn hier eine 0 in Rntime steht, solltre es keinen Raum in der Form geben. Bitte keine Werte eintragen, das wird in runtime eh überschrieben!")]
    public long shape0Board, shape1Board, shape2Board, shape3Board, shape4Board, shape5Board,
     shape6Board, shape7Board, shape8Board, shape9Board, shape10Board;

    [InfoBox("Ein Raum ohne Eingang. Stopft die Löcher, die entstehen...")]
    public long roadblockBoard;

    public RoomTemplate roadBlock;

    #endregion

    private string[] _moduleBoardStrings = new string[]
    {
        _moduleString1 = "0000000011000000010001000001010000001100000011000100100010001000",
        _moduleString2 = "0000000010001000010000110001000001000001100010000100010011100000",
    };
    
    private long[] _moduleBoard = new long[]
    {
        _moduleBoard0 = Convert.ToInt64(_moduleString1,2),
        _moduleBoard1 = Convert.ToInt64(_moduleString2,2)
    };

    #region Initializing RoomShapeArrays

    [InfoBox("In diese Arrays werden die RaumTemplates nach Raumform sortiert reingezogen!\n \n" + "Legende: \n" +
             "O = oben \n" + "U = unten \n" + "R = rechts \n" + "L = links \n")]
    [Header("PrefabArrays")]
    [SerializeField] private byte fucksGiven;
    
    [Header("- Ausgänge L/R")] public RoomTemplate[] room0Shapes;

    [Header("| Ausgänge O/U")] public RoomTemplate[] room1Shapes;

    [Header("└ Ausgänge O/R")] public RoomTemplate[] room2Shapes;

    [Header("┘ Ausgänge O/L")]public RoomTemplate[] room3Shapes;

    [Header("┌ Ausgänge U/R")]public RoomTemplate[] room4Shapes;

    [Header("┐ Ausgänge U/L")]public RoomTemplate[] room5Shapes;

    [Header("├ Ausgänge O/U/R")]public RoomTemplate[] room6Shapes;

    [Header("┤ Ausgänge O/U/L")]public RoomTemplate[] room7Shapes;

    [Header("┴ Ausgänge O/R/L")]public RoomTemplate[] room8Shapes;

    [Header("┬ Ausgänge U/R/L")]public RoomTemplate[] room9Shapes;

    [Header("┼ Ausgänge O/U/R/L")]public RoomTemplate[] room10Shapes;
    
    #endregion
    
    private BoardGenerator _mapGenerator;

    private int _visitedCellCount = 0;
    private bool[,] _visitedCells;

    [InfoBox("Hier ist ein Faktor hinterlegt, der die Gesamtfläche, die mit Räumen gefüllt werden muss anzeigt." +
    "\n" +
    "Wenn der Faktor nicht erreicht wird, wird das Board neu erstellt, es sollte rein theoretisch also nie kleiner als 1 sein.")]
    [Range(0,100)][SerializeField] private float minimumMazePercentage = 100;
    
    private static string _moduleString1;
    private static string _moduleString2;
    private static long _moduleBoard0 = 0;
    private static long _moduleBoard1 = 0;

    private long _chosenModuleBoard;
    private long _platformerBoard;
    private long _chillBoard;
    
    private void Awake()
    {
        Debug.LogWarning($"You gave {fucksGiven} fucks!");
        
        _chosenModuleBoard = _moduleBoard[Random.Range(0, _moduleBoard.Length)];
        _chillBoard = Random.Range(100, Int32.MaxValue);
        
        _mapGenerator = GetComponent<BoardGenerator>();
        _mapGenerator.InitializeRoomWithCharacters();
    }

    void Start()
    {
    DisplayBoard();
    #region Debug

    string shape1String = Convert.ToString(shape0Board, 2).PadLeft(64, '0');
    string shape2String = Convert.ToString(shape1Board, 2).PadLeft(64, '0');
    string shape3String = Convert.ToString(shape2Board, 2).PadLeft(64, '0');
    string shape4String = Convert.ToString(shape3Board, 2).PadLeft(64, '0');
    string shape5String = Convert.ToString(shape4Board, 2).PadLeft(64, '0');
    string shape6String = Convert.ToString(shape5Board, 2).PadLeft(64, '0');
    string shape7String = Convert.ToString(shape6Board, 2).PadLeft(64, '0');
    string shape8String = Convert.ToString(shape7Board, 2).PadLeft(64, '0');
    string shape9String = Convert.ToString(shape8Board, 2).PadLeft(64, '0');
    string shape10String = Convert.ToString(shape9Board, 2).PadLeft(64, '0');
    string shape11String = Convert.ToString(shape10Board, 2).PadLeft(64, '0');

    Debug.Log($"Shape 1 Board is {shape1String}");
    Debug.Log($"Shape 2 Board is {shape2String}");
    Debug.Log($"Shape 3 Board is {shape3String}");
    Debug.Log($"Shape 4 Board is {shape4String}");
    Debug.Log($"Shape 5 Board is {shape5String}");
    Debug.Log($"Shape 6 Board is {shape6String}");
    Debug.Log($"Shape 7 Board is {shape7String}");
    Debug.Log($"Shape 8 Board is {shape8String}");
    Debug.Log($"Shape 9 Board is {shape9String}");
    Debug.Log($"Shape 10 Board is {shape10String}");
    Debug.Log($"Shape 11 Board is {shape11String}");
    
    #endregion
    }

    private void DisplayBoard()
    {
        _visitedCells = new bool[_mapGenerator.mapRows, _mapGenerator.mapColumns];

        float minimumBoardTiles = Mathf.Round((_mapGenerator.mapRows - 2) * (_mapGenerator.mapColumns - 2) * minimumMazePercentage / 100);
        
        while (_visitedCellCount < minimumBoardTiles) 
        {
            _visitedCellCount = 0;
            _mapGenerator.InitializeBoard();
            _visitedCells = _mapGenerator.TraverseBoard();
            _visitedCellCount = _mapGenerator.GetVisitedCellCount(_visitedCells);
        }
        _mapGenerator.DisplayBoard();
        InstantiateBoard();
    }

    private List<RoomTemplate> GetValidModuleRooms(RoomTemplate[] roomTemplates)
    {
        List<RoomTemplate> validRoomsList = new List<RoomTemplate>();
        
        foreach (var template in roomTemplates)
        {
            if (template.roomType == RoomTemplate.RoomType.Module)
            {
                validRoomsList.Add(template);
            }
            else
            {
                continue;
            }
        }
        return validRoomsList;
    }

    private List<RoomTemplate> GetValidPlatformerRooms(RoomTemplate[] roomTemplates)
    {
        List<RoomTemplate> validRoomsList = new List<RoomTemplate>();
        
        foreach (var template in roomTemplates)
        {
            if (template.roomType == RoomTemplate.RoomType.Platform)
            {
                validRoomsList.Add(template);
            }
        }
        return validRoomsList;
    }
    
    private List<RoomTemplate> GetValidChillRooms(RoomTemplate[] roomTemplates)
    {
        List<RoomTemplate> validRoomsList = new List<RoomTemplate>();
        
        foreach (var template in roomTemplates)
        {
            if (template.roomType == RoomTemplate.RoomType.Chill)
            {
                validRoomsList.Add(template);
            }
        }
        return validRoomsList;
    }
     
    private void InstantiateBoard()
     {
            long takenRooms = 0;
            
            for (int r = 1; r < _mapGenerator.mapRows - 1; r++)
            {
                for (int c = 1; c < _mapGenerator.mapColumns - 1; c++)
                {
                    string ch = _mapGenerator.Board[r, c].ToString();
                    int charPos = _mapGenerator.roomCharacters.IndexOf(ch, StringComparison.Ordinal);

                    if (charPos < 0 || !_visitedCells[r, c])
                    {
                        continue;
                    }
                    
                    //TODO: Refactor List for all ShapeBoards +  charPos == i im loop
                    #region Setting the Boards purely for roomShapes

                    if (charPos == 0) shape0Board = SetCellState(shape0Board, r, c);
                    else if (charPos == 1) shape1Board = SetCellState(shape1Board, r, c);
                    else if (charPos == 2) shape2Board = SetCellState(shape2Board, r, c);
                    else if (charPos == 3) shape3Board = SetCellState(shape3Board, r, c);
                    else if (charPos == 4) shape4Board = SetCellState(shape4Board, r, c);
                    else if (charPos == 5) shape5Board = SetCellState(shape5Board, r, c);
                    else if (charPos == 6) shape6Board = SetCellState(shape6Board, r, c);
                    else if (charPos == 7) shape7Board = SetCellState(shape7Board, r, c);
                    else if (charPos == 8) shape8Board = SetCellState(shape8Board, r, c);
                    else if (charPos == 9) shape9Board = SetCellState(shape9Board, r, c);
                    else if (charPos == 10) shape10Board = SetCellState(shape10Board, r, c);
                    
                    #endregion
                    //TODO: Refactor List for all ShapesObjects + List for all shapeLists (possible?) und dann Index
                    
                    #region Init ModuleRooms

                    if (GetCellState(shape0Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room0Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);
                    }
                    if (GetCellState(shape1Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room1Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom =  Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState(shape2Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room2Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }

                    if (GetCellState(shape3Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room3Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState(shape4Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room4Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }

                    if (GetCellState(shape5Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room5Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $" Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState(shape6Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room6Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }

                    if (GetCellState(shape7Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room7Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState(shape8Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room8Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }

                    if (GetCellState(shape9Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room9Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState(shape10Board & _chosenModuleBoard,r,c))
                    {
                        var validRooms = GetValidModuleRooms(room10Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Moduleroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    #endregion

                    #region Init Platformer rooms
                    if (GetCellState((shape0Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room0Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight, 0), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape1Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        
                        var validRooms = GetValidPlatformerRooms(room1Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape2Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room2Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape3Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room3Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape4Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room4Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape5Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room5Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape6Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room6Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape7Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room7Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape8Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room8Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape9Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room9Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((shape10Board &~ _chosenModuleBoard) &~ _chillBoard,r,c))
                    {
                        var validRooms = GetValidPlatformerRooms(room10Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"PlatformerRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    #endregion
                    #region Init Chill rooms
                    
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape0Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room0Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape1Board,r,c))
                    {
                        
                        var validRooms = GetValidChillRooms(room1Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c* roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"ChillRoom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape2Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room2Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.name = $"Chillroom_{r},{c}";
                        newRoom.transform.parent = transform;
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape3Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room3Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.name = $"Chillroom_{r},{c}";
                        newRoom.transform.parent = transform;
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape4Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room4Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape5Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room5Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape6Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room6Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape7Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room7Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape8Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room8Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r * roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape9Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room9Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    if (GetCellState((_chillBoard &~ _chosenModuleBoard) & shape10Board,r,c))
                    {
                        var validRooms = GetValidChillRooms(room10Shapes);
                        int randomRoom = Random.Range(0, validRooms.Count);
                        RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r * roomHeight), Quaternion.identity);
                        newRoom.transform.parent = transform;
                        newRoom.name = $"Chillroom_{r},{c}";
                        takenRooms = SetCellState(takenRooms, r, c);                    }
                    #endregion

                    if (GetCellState(roadblockBoard,r,c))
                    {
                        RoomTemplate newRoadBlock = Instantiate(roadBlock, new Vector3(c * roomWidth, r * roomHeight),
                            roadBlock.transform.rotation);
                        newRoadBlock.transform.parent = transform;
                        newRoadBlock.name = $"Roadblock @ {r} | {c}";
                    }
                }
            }
            
            string takenRoomsString = Convert.ToString(takenRooms, 2).PadLeft(64, '0');

            long freeRooms = 0;
            Debug.Log(takenRoomsString);
            for (int r = 1; r < _mapGenerator.mapRows -1; r++)
            {
                for (int c = 1; c < _mapGenerator.mapColumns -1; c++)
                {
                    if (GetCellState(freeRooms &~ takenRooms,r,c))
                    {
                      if (GetCellState(shape0Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room0Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape1Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room1Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom =  Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape2Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room2Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }

                        if (GetCellState(shape3Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room3Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape4Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room4Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }

                        if (GetCellState(shape5Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room5Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $" Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape6Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room6Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }

                        if (GetCellState(shape7Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room7Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape8Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room8Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }

                        if (GetCellState(shape9Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room9Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                        if (GetCellState(shape10Board,r,c))
                        {
                            var validRooms = GetValidModuleRooms(room10Shapes);
                            int randomRoom = Random.Range(0, validRooms.Count);
                            RoomTemplate newRoom = Instantiate(validRooms[randomRoom], new Vector3(c * roomWidth, r* roomHeight), Quaternion.identity);
                            newRoom.transform.parent = transform;
                            newRoom.name = $"Inserted Moduleroom_{r},{c}";
                        }
                    }
                }
            }
     }

    private bool GetCellState(long bitboard, int row, int column)
     {
         long mask = 1L << ((row * 8) + column);
         return (bitboard & mask) !=0;
     }

    private long SetCellState(long bitboard, int row, int column)
     {
         long newTileState = 1L << ((row * 8) + column);
         return (bitboard |= newTileState);
     }

     #region Debug

     public int CellCount(long bitboard)
     {
         int count = 0;
         long bb = bitboard;

         while (bb != 0)
         {
             bb &= bb - 1;
             count++;
         }

         return count;
     }

     #endregion
}
