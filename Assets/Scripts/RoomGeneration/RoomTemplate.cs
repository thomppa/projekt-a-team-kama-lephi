﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplate : MonoBehaviour
{
    private TemplateManager _templateManager;
    
    public enum RoomType
    {
        Module,
        Trap,
        Platform,
        Chill,
    }
    public RoomType roomType;
    
    private void Awake()
    {
       _templateManager = TemplateManager.Instance;
    }
}
