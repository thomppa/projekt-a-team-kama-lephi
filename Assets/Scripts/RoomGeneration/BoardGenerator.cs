﻿using System.Runtime.InteropServices;
using NaughtyAttributes;
using UnityEngine;

//Contributors: Kay
public class BoardGenerator : MonoBehaviour {
	
	[InfoBox("Bei allen Werten im Kopf bitte 2 abziehen, da die äußeren Grenzen des Spielfelds zwar Reihe und Spalte sind," +
	         "aber nicht mit Räumen instantiated werden, so weiß der Generator aber, wo die Grenzen des Boards sind.")]
	public int mapRows = 5;
	public int mapColumns = 10;

	public char[,] Board;

	public string roomCharacters;
	private string[] _roomsThatFitAbove;
	private string[] _roomsThatFitBelow;
	private string[] _roomsThatFitLeft;
	private string[] _roomsThatFitRight;

	public void DisplayBoard() {
		string output = "";
		for (int r = 0; r < mapRows; r++) {
			for (int c = 0; c < mapColumns; c++) {
				output += Board [r, c];
			}
			output += "\n";
		}
		Debug.Log (output);
	}

	public void InitializeBoard() {
		Board = new char[mapRows, mapColumns];

		// Put 'X's in top and bottom rows.
		for (int c = 0; c < mapColumns; c++) {
			Board [0, c] = 'X';
			Board [mapRows - 1, c] = 'X';
		}

		// Put 'X's in the left and right columns.
		for (int r = 0; r < mapRows; r++) {
			Board [r, 0] = 'X';
			Board [r, mapColumns - 1] = 'X';
		}

		// Set 'O' for the other map spaces (which means 'free').
		for (int r = 1; r < mapRows - 1; r++) {
			for (int c = 1; c < mapColumns - 1; c++) {
				Board [r, c] = 'O';
			}
		}
		Random.seed = System.DateTime.Now.Millisecond;
			
		for (int r = 1; r < mapRows - 1; r++) {
			for (int c = 1; c < mapColumns - 1; c++) {

				if (Board [r, c] == '@') {
					continue;
				}
				string validCharacters = GetValidRoomCharacters (r, c);
				Board [r, c] = validCharacters [Random.Range (0, validCharacters.Length)];
			}
		}
	}
	private string GetValidRoomCharacters(int row, int column) {
		string validCharacters = "";

		for (int i = 0; i < roomCharacters.Length; i++) {
			char ch = roomCharacters [i];

			if (
				_roomsThatFitLeft [i].Contains (Board [row, column - 1].ToString ()) &&
				_roomsThatFitRight [i].Contains (Board [row, column + 1].ToString ()) &&
				_roomsThatFitAbove [i].Contains (Board [row - 1, column].ToString ()) &&
				_roomsThatFitBelow [i].Contains (Board [row + 1, column].ToString ()))
			{
				validCharacters += ch.ToString ();
			}
		}

		if (validCharacters.Length == 0) {
			validCharacters = "O";
		}
		return validCharacters;
	}

	public bool[,] TraverseBoard()
	{
		bool[,] visitedCells = new bool[mapRows,mapColumns];

		int _currentRows = 1;
		int _currentColumns = 1;
		
		CheckCellIfVisited(visitedCells,_currentRows, _currentColumns);

		#region Debug

		// for (int r = 0; r < mapRows-1; r++)
		// {
		// 	for (int c = 0; c < mapColumns-1; c++)
		// 	{
		// 		Debug.Log("Cell visited" +r+","+c +" = "+visitedCells[r,c]);
		// 	}
		// }

		#endregion
		
		return visitedCells;
	}

	private void CheckCellIfVisited(bool[,] visitedCells, int row, int column)
	{
		if (visitedCells[row,column])
		{
			return;
		}

		visitedCells[row, column] = true;

		switch (Board[row,column])
		{
			case '┌':
				CheckCellIfVisited(visitedCells,row,column+1); 
				CheckCellIfVisited(visitedCells,row+1,column); 
				break;
			case '┐':
				CheckCellIfVisited(visitedCells,row,column-1); 
				CheckCellIfVisited(visitedCells,row+1,column); 
				break;
			case '─':
				CheckCellIfVisited(visitedCells,row,column+1);
				CheckCellIfVisited(visitedCells,row,column-1);
				break;
			case '│':
				CheckCellIfVisited(visitedCells,row-1,column);
				CheckCellIfVisited(visitedCells,row+1,column);
				break;
			case '└':
				CheckCellIfVisited(visitedCells,row,column+1);
				CheckCellIfVisited(visitedCells,row-1,column);
				break;
			case '┘':
				CheckCellIfVisited(visitedCells,row,column-1);
				CheckCellIfVisited(visitedCells,row-1,column);
				break;
			case '├':
				CheckCellIfVisited(visitedCells,row-1,column);
				CheckCellIfVisited(visitedCells,row,column+1);
				CheckCellIfVisited(visitedCells,row+1,column);
				break;
			case '┤':
				CheckCellIfVisited(visitedCells,row-1,column);
				CheckCellIfVisited(visitedCells,row,column-1);
				CheckCellIfVisited(visitedCells,row+1,column);
				break;
			case '┬':
				CheckCellIfVisited(visitedCells,row,column-1);
				CheckCellIfVisited(visitedCells,row+1,column);
				CheckCellIfVisited(visitedCells,row,column+1);
				break;
			case '┴':
				CheckCellIfVisited(visitedCells,row,column-1);
				CheckCellIfVisited(visitedCells,row-1,column);
				CheckCellIfVisited(visitedCells,row,column+1);
				break;
			case '┼':
				CheckCellIfVisited(visitedCells,row-1,column);
				CheckCellIfVisited(visitedCells,row+1,column);
				CheckCellIfVisited(visitedCells,row,column-1);
				CheckCellIfVisited(visitedCells,row,column+1);
				break;
			case 'O':
				return;
			default:
				Debug.LogError("Unbekannte Form: "+row +"," +column);
				return;
		}
	}

	public int GetVisitedCellCount(bool[,] visitedCells)
	{
		int visitedCellCount = 0;

		for (int r = 0; r < mapRows; r++)
		{
			for (int c = 0; c < mapColumns; c++)
			{
				if (visitedCells[r,c])
				{
					visitedCellCount++;
				}
			}
		}
		return visitedCellCount;
	}


	public void InitializeRoomWithCharacters() {
		roomCharacters = "─│┌┐└┘├┤┬┴┼"; 
		_roomsThatFitAbove = new string[roomCharacters.Length];
		_roomsThatFitBelow = new string[roomCharacters.Length];
		_roomsThatFitLeft = new string[roomCharacters.Length];
		_roomsThatFitRight = new string[roomCharacters.Length];

		_roomsThatFitLeft [0] = "O─┌└├┬┴┼"; //    ─
		_roomsThatFitLeft [1] = "O│┐┘┤X"; //     │
		_roomsThatFitLeft [2] = "O│┐┘┤X"; //     ┌
		_roomsThatFitLeft [3] = "O─┌└├┬┴┼"; //    ┐
		_roomsThatFitLeft [4] = "O│┐┘┤X"; //     └
		_roomsThatFitLeft [5] = "O─┌└├┬┴┼"; //    ┘
		_roomsThatFitLeft [6] = "O│┐┘┤X"; //      ├
		_roomsThatFitLeft [7] = "O─┌└├┬┴┼"; //   ┤
		_roomsThatFitLeft [8] = "O─┌└├┬┴┼"; //    ┬
		_roomsThatFitLeft [9] = "O─┌└├┬┴┼"; //    ┴
		_roomsThatFitLeft [10] = "O─┌└├┬┴┼"; //   ┼

		_roomsThatFitRight [0] = "O─┐┘┤┬┴┼"; //    ─
		_roomsThatFitRight [1] = "O│┌└├X"; //     │
		_roomsThatFitRight [2] = "O─┐┘┤┬┴┼"; //   ┌
		_roomsThatFitRight [3] = "O│┌└├X"; //      ┐
		_roomsThatFitRight [4] = "O─┐┘┤┬┴┼"; //   └
		_roomsThatFitRight [5] = "O│┌└├X"; //      ┘
		_roomsThatFitRight [6] = "O─┐┘┤┬┴┼"; //   ├
		_roomsThatFitRight [7] = "O│┌└├X"; //      ┤
		_roomsThatFitRight [8] = "O─┐┘┤┬┴┼"; //    ┬
		_roomsThatFitRight [9] = "O─┐┘┤┬┴┼"; //    ┴
		_roomsThatFitRight [10] = "O─┐┘┤┬┴┼"; //   ┼

		_roomsThatFitAbove [0] = "O─└┘┴X"; //       ─
		_roomsThatFitAbove [1] = "O│┌┐├┤┬┼"; //      │
		_roomsThatFitAbove [2] = "O─└┘┴X"; //        ┌
		_roomsThatFitAbove [3] = "O─└┘┴X"; //        ┐
		_roomsThatFitAbove [4] = "O│┌┐├┤┬┼"; //     └
		_roomsThatFitAbove [5] = "O│┌┐├┤┬┼"; //     ┘
		_roomsThatFitAbove [6] = "O│┌┐├┤┬┼"; //      ├
		_roomsThatFitAbove [7] = "O│┌┐├┤┬┼"; //      ┤
		_roomsThatFitAbove [8] = "O─└┘┴X"; //        ┬
		_roomsThatFitAbove [9] = "O│┌┐├┤┬┼"; //     ┴
		_roomsThatFitAbove [10] = "O│┌┐├┤┬┼"; //     ┼

		_roomsThatFitBelow [0] = "O─┌┐┬X"; //       ─
		_roomsThatFitBelow [1] = "O│└┘├┤┴┼"; //      │
		_roomsThatFitBelow [2] = "O│└┘├┤┴┼"; //     ┌
		_roomsThatFitBelow [3] = "O│└┘├┤┴┼"; //     ┐
		_roomsThatFitBelow [4] = "O─┌┐┬X"; //        └
		_roomsThatFitBelow [5] = "O─┌┐┬X"; //        ┘
		_roomsThatFitBelow [6] = "O│└┘├┤┴┼"; //      ├
		_roomsThatFitBelow [7] = "O│└┘├┤┴┼"; //      ┤
		_roomsThatFitBelow [8] = "O│└┘├┤┴┼"; //     ┬
		_roomsThatFitBelow [9] = "O─┌┐┬X"; //        ┴
		_roomsThatFitBelow [10] = "O│└┘├┤┴┼"; //     ┼
	}

}