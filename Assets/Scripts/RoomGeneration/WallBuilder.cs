﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NaughtyAttributes;

[RequireComponent(typeof(Room))]
public class WallBuilder : MonoBehaviour
{
    [SerializeField] GameObject prefabDoor;
    [SerializeField] GameObject prefabWall;
    [Space, SerializeField] WallType ceiling;
    [SerializeField] WallType leftWall;
    [SerializeField] WallType rightWall;
    [SerializeField] WallType floor;

    enum WallType { Wall, Door }

    void Start()
    {
        //ReplaceWalls(gameObject.transform);
    }

    public bool IsDoorInDirection(Vector2 direction)
    {
        if (direction.normalized == Vector2.up) return ceiling == WallType.Door;
        if (direction.normalized == Vector2.left) return leftWall == WallType.Door;
        if (direction.normalized == Vector2.right) return rightWall == WallType.Door;
        if (direction.normalized == Vector2.down) return floor == WallType.Door;
        return false;
    }

#if UNITY_EDITOR
    [Button("Update Walls", EButtonEnableMode.Always)]
    void UpdateWalls()
    {
        // check if object is prefab or standalone/in prefab mode
        if (PrefabUtility.GetCorrespondingObjectFromSource(gameObject))
        {
            // load prefab contents of this gameObject
            string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(gameObject);
            GameObject contentsRoot = PrefabUtility.LoadPrefabContents(prefabPath);

            ReplaceWalls(contentsRoot.transform);

            // Save contents back to prefab and unload contents
            PrefabUtility.SaveAsPrefabAsset(contentsRoot, prefabPath);
            PrefabUtility.UnloadPrefabContents(contentsRoot);
        }
        else ReplaceWalls(gameObject.transform);
    }
#endif

    void ReplaceWalls(Transform root)
    {
        // destroy existing walls if container object is found
        Transform essentials = root.Find("RoomEssentials");
        if (!essentials) Debug.LogError("The container object 'RoomEssentials' can not be found inside the prefab.");
        if (essentials.Find("WallContainer")) DestroyImmediate(essentials.Find("WallContainer").gameObject);

        // create new WallContainer inside the essentials container
        Transform container = new GameObject("WallContainer").transform;
        container.parent = essentials;
        container.localPosition = Vector3.zero;

        // instantiate new walls inside WallContainer
        Vector2 roomSize = GetComponent<Room>().Size;
        NewWallInstance(ceiling, 0, roomSize.y / 2, true, roomSize, container);
        NewWallInstance(leftWall, 90, -roomSize.x / 2, false, roomSize, container);
        NewWallInstance(rightWall, -90, roomSize.x / 2, false, roomSize, container);
        NewWallInstance(floor, 180, -roomSize.y / 2, true, roomSize, container);
    }

    void NewWallInstance(WallType type, float zRot, float offset, bool isHorizontal, Vector2 roomSize, Transform parent)
    {
        Vector3 scale = isHorizontal ? new Vector3(roomSize.x / 10, 1, 1) : new Vector3(roomSize.y / 10, 1, 1);
        Vector3 position = isHorizontal ? new Vector3(0, offset, 0) : new Vector3(offset, 0, 0);

        GameObject prefab = type == WallType.Wall ? prefabWall : prefabDoor;
        Instantiate(prefab, parent.position + position, Quaternion.Euler(0, 0, zRot), parent).transform.localScale = scale;
    }
}