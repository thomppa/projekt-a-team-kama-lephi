﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class PrefabRootSelectorEditor
{
    [MenuItem("MP Tools/Select Prefab Root _SPACE")]
    static void SelectPrefabRoot()
    {
        int[] modifiedSelectionIDs = Selection.instanceIDs;
        foreach (GameObject go in Selection.gameObjects)
            TryReplaceSelectionWithPrefabRoot(go, ref modifiedSelectionIDs);
        Selection.instanceIDs = modifiedSelectionIDs;
    }

    static void TryReplaceSelectionWithPrefabRoot(GameObject activeSelection, ref int[] modifiedSelectionIDs)
    {
        if (activeSelection && PrefabUtility.IsPartOfAnyPrefab(activeSelection) && !PrefabUtility.IsAnyPrefabInstanceRoot(activeSelection))
        {
            GameObject prefabRoot = PrefabUtility.GetNearestPrefabInstanceRoot(activeSelection);
            if (prefabRoot && !prefabRoot.CompareTag("Room"))
                for (int i = 0; i < modifiedSelectionIDs.Length; i++)
                    if (modifiedSelectionIDs[i] == activeSelection.GetInstanceID())
                    {
                        modifiedSelectionIDs[i] = prefabRoot.GetInstanceID();
                        break;
                    }
        }
    }
}