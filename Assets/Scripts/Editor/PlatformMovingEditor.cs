﻿using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

[CustomEditor(typeof(PlatformMoving)), CanEditMultipleObjects]
public class PlatformMovingEditor : Editor
{
    void OnSceneGUI()
    {
        PlatformMoving platform = (PlatformMoving)target;

        EditorGUI.BeginChangeCheck();
        // draw positionHandle in world space
        Vector3 newTargetPosition = Handles.PositionHandle(platform.MoveTargetPosition, Quaternion.identity);

        // draw dotted line
        Handles.color = Color.red;
        Handles.zTest = CompareFunction.Less;
        Handles.DrawDottedLine(platform.transform.position, newTargetPosition, 7);
        Handles.DrawWireCube(platform.MoveTargetPosition, Vector3.Scale(platform.MovingObject.localScale, platform.transform.localScale));

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(platform, "Change Look At Target Position");
            // store positionHandle position in local space
            platform.localMoveTargetPosition = platform.transform.InverseTransformPoint(newTargetPosition);
        }
    }
}
