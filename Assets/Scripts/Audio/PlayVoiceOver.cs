﻿
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayVoiceOver : MonoBehaviour
{
    private AudioSource audioSource;
    private bool alreadyPlayed = false;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(!alreadyPlayed)
            {
                audioSource.Play();
                alreadyPlayed = true;
            }
            
           // Destroy(gameObject, audioSource.clip.length+1);
        }
    }
}
