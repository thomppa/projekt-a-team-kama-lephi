﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ButtonSounds : MonoBehaviour
{
    [SerializeField] AudioClip buttonHover;
    [SerializeField] AudioClip buttonPress;

    AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.ignoreListenerPause = true;
    }

    public void PlayButtonPress()
    {
        if (audioSource.enabled)
        {
            audioSource.clip = buttonPress;
            audioSource.Play();
        }
    }

    public void PlayButtonHover()
    {
        if (audioSource.enabled)
        {
            audioSource.clip = buttonHover;
            audioSource.Play();
        }
    }
}
