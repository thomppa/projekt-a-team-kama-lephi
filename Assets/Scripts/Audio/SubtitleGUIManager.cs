﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SubtitleGUIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textBox;

    public void Clear()
    {
        textBox.text = string.Empty;
    }
    public void SetText(string text)
    {
        textBox.text = text;
    }
}
