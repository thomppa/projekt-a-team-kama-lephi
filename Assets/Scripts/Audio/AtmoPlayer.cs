﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AtmoPlayer : MonoBehaviour
{
    [SerializeField] int cycleAtmoStartRoomIndex = 5;
    [SerializeField] bool playWhenGamePaused = true;

    bool isCycleAtmoPlaying;
    bool isTutorialAtmoPlaying;

    void Start()
    {
        if (playWhenGamePaused) GetComponent<AudioSource>().ignoreListenerPause = true;
    }

    public void UpdateAtmoSounds()
    {
        if (SceneStreamingManager.Instance.CurrentSceneIndex < cycleAtmoStartRoomIndex)
        {
            if (!isTutorialAtmoPlaying)
            {
                isTutorialAtmoPlaying = true;
                StartCoroutine(PlayAtmoSound("TutorialStart", "TutorialLoop"));
            }
        }
        else if (!isCycleAtmoPlaying)
        {
            isCycleAtmoPlaying = true;
            AudioManager.instance.StopSound("TutorialStart");
            AudioManager.instance.StopSound("TutorialLoop");
            StartCoroutine(PlayAtmoSound("CycleStart", "CycleLoop"));
        }
    }

    private IEnumerator PlayAtmoSound(string soundStartName, string soundLoopName)
    {
        AudioManager.instance.PlaySound(soundStartName);
        yield return new WaitForSeconds(AudioManager.instance.GetClip(soundStartName).length - 0.7f);
        AudioManager.instance.PlaySound(soundLoopName);
    }
}
