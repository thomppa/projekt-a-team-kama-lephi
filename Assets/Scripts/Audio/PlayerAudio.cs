﻿using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip footstep;

    [SerializeField]
    private AudioClip swimMotion;

    public AudioSource audioSource;
    public AudioSource audioSource2;

    public void Step()
    {
        audioSource.PlayOneShot(footstep);
    }

    public void Swim()
    {
        audioSource2.PlayOneShot(swimMotion);
    }

}
