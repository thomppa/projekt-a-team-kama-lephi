﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(AudioSource))]
public class PlaySubtitle : MonoBehaviour
{
    private AudioSource audioSource;
    private VoiceScriptManager scriptManager;
    private SubtitleGUIManager guiManager;
    private bool alreadyPlayed = false;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        scriptManager = FindObjectOfType<VoiceScriptManager>();
        guiManager = FindObjectOfType<SubtitleGUIManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!alreadyPlayed)
            {

                StartCoroutine(DisplaySubtitle());
            }

           
        }
    }

    private IEnumerator DisplaySubtitle()
    {
        var script = scriptManager.GetText(audioSource.clip.name);
        guiManager.SetText(script);

        yield return new WaitForSeconds(audioSource.clip.length);
        guiManager.SetText(string.Empty);

        alreadyPlayed = true;
    }
      
}
