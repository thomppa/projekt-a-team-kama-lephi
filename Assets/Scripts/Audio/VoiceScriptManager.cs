﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VoiceScriptManager : MonoBehaviour
{

    private Dictionary<string, string> lines = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

    public string resourceFile = "script";

    public string GetText(string textKey)
    {
        string tmp = "";
        if (lines.TryGetValue(textKey, out tmp))
            return tmp;
        return "<color=#ff00ff>MISSING TEXT</color>";

    }
    private void Awake()
    {
        var textAsset = Resources.Load<TextAsset>(resourceFile);
        var voText = JsonUtility.FromJson<VoiceOverText>(textAsset.text);

        foreach(var t in voText.lines)
        {
            lines[t.key] = t.line;
        }
    }
}
