﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ModuleEffectDisplay : MonoBehaviour
{
    [SerializeField, OnValueChanged("UpdateEffectVisibility")] bool showWaterEffect = true;
    [SerializeField, OnValueChanged("UpdateEffectVisibility")] bool showWindEffect = true;
    [SerializeField, OnValueChanged("UpdateEffectVisibility")] bool showFireEffect = true;

    [SerializeField, Foldout("Advanced Properties")] GameObject waterEffectObj;
    [SerializeField, Foldout("Advanced Properties")] GameObject windEffectObj;
    [SerializeField, Foldout("Advanced Properties")] GameObject fireEffectObj;

    void Awake()
    {
        UpdateEffectVisibility();
    }

    void UpdateEffectVisibility()
    {
        waterEffectObj.SetActive(showWaterEffect);
        windEffectObj.SetActive(showWindEffect);
        fireEffectObj.SetActive(showFireEffect);
    }
}
