﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[ExecuteInEditMode]
public class ModelUtilities : MonoBehaviour
{
    [SerializeField, ReorderableList] List<ObjectVisibility> objectVisibility = new List<ObjectVisibility>();

    private void Update()
    {
        if (Application.isEditor)
            foreach (var ov in objectVisibility) ov.CheckForChange();
    }
}

[System.Serializable]
public class ObjectVisibility
{
    public bool isVisible = true;
    public GameObject gameObject;

    bool wasVisible;

    public void CheckForChange()
    {
        if (wasVisible != isVisible)
        {
            wasVisible = isVisible;
            UpdateVisibility();
        }
    }

    void UpdateVisibility()
    {
        if (gameObject) gameObject.SetActive(isVisible);
    }
}
#endif