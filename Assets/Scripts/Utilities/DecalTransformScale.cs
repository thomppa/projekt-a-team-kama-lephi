﻿using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[ExecuteInEditMode, RequireComponent(typeof(DecalProjector))]
public class DecalTransformScale : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<DecalProjector>().size = transform.localScale;
    }

    void OnDrawGizmosSelected()
    {
        GetComponent<DecalProjector>().size = transform.localScale;
    }
}
