﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerActivatableEventTrigger : PowerActivatable
{
    [SerializeField] UnityEvent onPowerActive = new UnityEvent();

    protected override void OnPowerSet(bool powerActive)
    {
        if (powerActive) onPowerActive?.Invoke();
    }
}
