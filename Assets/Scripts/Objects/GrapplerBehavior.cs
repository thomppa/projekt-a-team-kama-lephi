﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplerBehavior : MonoBehaviour
{
    [SerializeField] Transform target;

    void Update()
    {
        if (target)
        {
            Vector3 relativePos = target.position - transform.position;

            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = rotation;
        }
    }
}
