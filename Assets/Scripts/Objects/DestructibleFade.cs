﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleFade : MonoBehaviour
{
    /// <param name="fadeTimer">The time waited until the objects will be faded.</param>
    public void Initialize(float fadeTimer)
    {
        StartCoroutine(Fade(fadeTimer));
    }

    IEnumerator Fade(float fadeTimer)
    {
        // disable collision on destructed meshes after time
        yield return new WaitForSeconds(fadeTimer);
        foreach (Transform t in transform)
            if (t.TryGetComponent(out Collider c)) c.enabled = false;

        // destroy destructed after time
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
