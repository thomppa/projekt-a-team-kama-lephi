﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProducedRobot : MonoBehaviour
{
    [SerializeField] GameObject Head;
    [SerializeField] GameObject LeftArm;
    [SerializeField] GameObject RightArm;
    [SerializeField] GameObject LeftLeg;
    [SerializeField] GameObject RightLeg;

    [SerializeField] bool headMounted;
    [SerializeField] bool leftArmMounted;
    [SerializeField] bool rightArmMounted;
    [SerializeField] bool leftLegMounted;
    [SerializeField] bool rightLegMounted;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(headMounted)
        {
            Head.SetActive(true);
        }
        else
        {
            Head.SetActive(false);
        }
        if(leftArmMounted)
        {
            LeftArm.SetActive(true);
        }
        else
        {
            LeftArm.SetActive(false);
        }        
        if(rightArmMounted)
        {
            RightArm.SetActive(true);
        }
        else
        {
            RightArm.SetActive(false);
        }
        if(leftLegMounted)
        {
            LeftLeg.SetActive(true);
        }
        else
        {
            LeftLeg.SetActive(false);
        }        
        if(rightLegMounted)
        {
            RightLeg.SetActive(true);
        }
        else
        {
            RightLeg.SetActive(false);
        }

    }
}
