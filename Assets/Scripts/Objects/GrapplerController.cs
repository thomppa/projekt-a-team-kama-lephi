﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplerController : PowerActivatable
{
    Animator animator;

    [SerializeField] GameObject carriedComponent;

    [SerializeField] bool carriesComponent = false;

    [SerializeField] bool high = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PowerActive)
        {
            high = true;
        }
        else if (PowerActive == false)
        {
            high = false;
        }

        if (animator)
        {
            if (high)
            {
                animator.SetBool("High", true);
            }
            else if (high == false)
            {
                animator.SetBool("High", false);
            }
        }

        if (carriedComponent)
        {
            if (carriesComponent == true) carriedComponent?.SetActive(true);
            else if (carriesComponent == false) carriedComponent?.SetActive(false);
        }
    }
}
