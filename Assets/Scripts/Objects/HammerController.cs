﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerController : PowerActivatable
{
    Animator animator;

    [SerializeField]
    private AudioClip impact;

    public AudioSource audioSource;

    [SerializeField] bool startActive;

    [SerializeField] float delayTime;
    [SerializeField] float timeProgress;
    [SerializeField] bool delayOver = false;

    private void Start()
    {
        animator = GetComponent<Animator>();

        animator.SetBool("Active", true);

        animator.enabled = false;
    }

    IEnumerator WaitForDelay()
    {
        yield return new WaitForSeconds(delayTime);

        animator.enabled = true;
    }

    public void TriggerSound()
    {
        if (impact != null && audioSource) audioSource.PlayOneShot(impact);
    }

    void Update()
    {
        /*if(timeProgress >= delayTime)
        {
            delayOver = true;
            timeProgress = 0f;
        }*/

        if (startActive)
        {
            StartActive();
        }
        else
        {
            Activatable();
        }
    }

    void StartActive()
    {
        StartCoroutine(WaitForDelay());

        /*if (delayOver == false)
        {
            timeProgress += 0.4f * Time.deltaTime;
        }

        if (delayOver == true)
        {
            animator.SetBool("Active", true);
        }*/
    }

    void Activatable()
    {
        if (PowerActive)
        {
            StartCoroutine(WaitForDelay());
        }
        else
        {
            StopCoroutine(WaitForDelay());
            animator.enabled = false;
        }

        /*{
   if (delayOver == false)
       {
          StartCoroutine(WaitForDelay());
          timeProgress += 0.1f * Time.deltaTime;
       }

       if(delayOver == true)
       {
           animator.SetBool("Active", true);
       }
   }
   else
   {
       animator.SetBool("Active", false);
       timeProgress = 0f;
   }*/
    }
}
