﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBeltTransport : PowerActivatable
{
    [SerializeField] GameObject TransportedObject;

    [SerializeField] bool movingRight;
    [SerializeField] bool movingLeft;
    [SerializeField] bool oneCycle = false;

    [SerializeField] int transportLenght = 10;
    [SerializeField] float transportSpeed = 4;
    
    [SerializeField] float delayTime;

    [SerializeField] bool directionLeft = true; 

    void Update()
    {
        if(directionLeft)
        {
            if (PowerActive)
            {
                movingLeft = true;
            }
            if (PowerActive == false)
            {
                movingLeft = false;
            }
        }

        if (directionLeft == false)
        {
            if (PowerActive)
            {
                movingRight = true;
            }
            if (PowerActive == false)
            {
                movingRight = false;
            }
        }

        if (movingLeft)
            transform.position += new Vector3(-transportSpeed, 0, 0) * Time.deltaTime;

        if(movingRight)
            transform.position += new Vector3(transportSpeed, 0, 0) * Time.deltaTime;

        if(transform.localPosition.x >= transportLenght)
        {
            if(oneCycle)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.localPosition = new Vector3(0, 0, 0);
            }
        }        
        if(transform.localPosition.x <= -transportLenght)
        {
            if (oneCycle)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.localPosition = new Vector3(0, 0, 0);
            }
        }
    }
}
