﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearRotation : PowerActivatable
{
    [SerializeField] float rotationStrength = 10;

    [SerializeField] bool startActive = false;

    [SerializeField] bool rotatingLeft = false;

    bool rotating;

    int orientation = 1;

    void Start()
    {
        if (rotatingLeft == false)
        {
            orientation = -1;
        }
    }

    void Update()
    {
        if (PowerActive || startActive)
        {
            rotating = true;
        }
        else
        {
            rotating = false;
        }

        if(rotating)
        {
            transform.Rotate(0, rotationStrength * Time.deltaTime * orientation, 0);
        }
    }
}
