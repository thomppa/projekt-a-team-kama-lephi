﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class Destructible : MonoBehaviour
{
    [SerializeField] GameObject prefabFractured;
    [SerializeField] float fracturesFadeTimer = 3;
    [SerializeField] Material burningMat;

    [Button("Destruct", EButtonEnableMode.Playmode)]
    void ForceDestruct() { Destruct(true); }

    private void Start()
    {
        tag = "Destructible";
    }

    public void Destruct(bool burn)
    {
        if (prefabFractured)
        {
            GameObject goInstance = Instantiate(prefabFractured, transform.position, transform.rotation);
            goInstance.transform.localScale = transform.localScale;
            SceneManager.MoveGameObjectToScene(goInstance, gameObject.scene);

            foreach (Transform t in goInstance.transform)
            {
                GameObject go = t.gameObject;

                if (burn)
                {
                    GameObject fire = Instantiate(go, go.transform);
                    fire.GetComponent<MeshRenderer>().material = burningMat;
                    fire.transform.localPosition = Vector3.zero;
                    fire.transform.localRotation = Quaternion.Euler(Vector3.zero);
                }

                go.AddComponent<BoxCollider>();
                Rigidbody r = go.AddComponent<Rigidbody>();
                r.constraints = RigidbodyConstraints.FreezePositionZ;
                r.sleepThreshold = 0;
                go.layer = LayerMask.NameToLayer("IgnorePlayer");
            }
            goInstance.AddComponent<DestructibleFade>().Initialize(fracturesFadeTimer);
        }

        Destroy(gameObject);
    }
}
