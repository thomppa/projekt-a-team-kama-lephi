﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class PlatformMoving : PowerActivatable
{
    [Space, SerializeField] public Vector3 localMoveTargetPosition;
    [SerializeField] MovementPattern movementPattern = MovementPattern.MoveAlways;
    [SerializeField] float travelTime = 1;
    [SerializeField] Transform movingObject;

    public Vector3 MoveTargetPosition { get { return transform.TransformPoint(localMoveTargetPosition); } }
    public Transform MovingObject { get => movingObject; }

    enum MovementPattern { MoveAlways, MoveOnActivation, MoveOnActivationReturnOtherwise, MoveOnActivationOnce }

    float travelTimer;
    bool moveForward = true;
    bool movingOnce;


    private void FixedUpdate()
    {
        float updatedTimer = travelTimer;
        switch (movementPattern)
        {
            case MovementPattern.MoveAlways:
                updatedTimer = UpdateAlwaysMoveTimer(); break;

            case MovementPattern.MoveOnActivation:
                if (PowerActive) updatedTimer = UpdateAlwaysMoveTimer(); break;

            case MovementPattern.MoveOnActivationReturnOtherwise:
                updatedTimer = PowerActive ? travelTimer + Time.deltaTime : travelTimer - Time.deltaTime; break;

            case MovementPattern.MoveOnActivationOnce:
                if (PowerActive) movingOnce = true;
                if (moveForward && movingOnce)
                {
                    updatedTimer = travelTimer + Time.deltaTime;
                    if (updatedTimer >= travelTime) moveForward = false;
                }
                break;
        }
        travelTimer = Mathf.Clamp(updatedTimer, 0, travelTime);
        movingObject.position = Vector3.Lerp(transform.position, MoveTargetPosition, travelTimer / travelTime);
    }

    private float UpdateAlwaysMoveTimer()
    {
        float value = moveForward ? travelTimer + Time.deltaTime : travelTimer - Time.deltaTime;
        if (value >= travelTime || value <= 0) moveForward = !moveForward;
        return value;
    }
}
