﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class SplashSceneLoader : MonoBehaviour
{
    [SerializeField, Scene] string mainMenuScene;
    [SerializeField] float waitTime = 5;
    [SerializeField] CanvasGroup group;

    private void Awake()
    {
        StartCoroutine(WaitForSplash());
    }

    IEnumerator WaitForSplash()
    {
        yield return new WaitForSecondsRealtime(waitTime);
        float t = 1;
        while (t > 0)
        {
            t -= Time.deltaTime;
            group.alpha = t;
            yield return null;
        }
        SceneManager.LoadScene(mainMenuScene);
    }
}