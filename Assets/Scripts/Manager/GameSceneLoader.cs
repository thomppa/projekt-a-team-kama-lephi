﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class GameSceneLoader : MonoBehaviour
{
    [SerializeField, Scene] string persistentGameScene;

    bool loadingScene;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void LoadGameScene(int spawnRoomIndex)
    {
        if (!loadingScene) StartCoroutine(LoadGameSceneAsync(spawnRoomIndex));
    }

    IEnumerator LoadGameSceneAsync(int spawnRoomIndex)
    {
        // load persisten scene
        loadingScene = true;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(persistentGameScene);
        while (!asyncLoad.isDone) yield return null;

        // persisten scene is loaded
        SceneStreamingManager.Instance.InitialSceneLoad(spawnRoomIndex);
        Destroy(gameObject);
    }
}