﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

//Contributors: Kay
//Tagmanager to create tags in Editormode, so teammembers don't have to create tags on their own
namespace Manager
{
    [ExecuteInEditMode]
    public class TagManager : MonoBehaviour
    {
        private void Awake()
        {
            SerializedObject tagManager =
                new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);

            SerializedProperty tagsProp = tagManager.FindProperty("tags");

            #region Tags to Add

            AddTag(tagsProp, "Tile");
            AddTag(tagsProp, "FireRoom");
            AddTag(tagsProp, "WindRoom");
            AddTag(tagsProp, "PlantRoom");
            AddTag(tagsProp, "ModuleRoom");
            AddTag(tagsProp, "WaterRoom");
            AddTag(tagsProp, "Trap");
            AddTag(tagsProp, "Start");
            AddTag(tagsProp, "TrapTrigger");

            #endregion

            tagManager.ApplyModifiedProperties();
        }

        public void AddTag(SerializedProperty tagsProp, string newTag)
        {
            bool found = false;

            //Check if tag is already existing
            for (int i = 0; i < tagsProp.arraySize; i++)
            {
                SerializedProperty t = tagsProp.GetArrayElementAtIndex(i);
                if (t.stringValue.Equals(newTag))
                {
                    found = true;
                    break;
                }
            }

            //add the new Tag

            if (!found)
            {
                tagsProp.InsertArrayElementAtIndex(0);
                SerializedProperty newTagProp = tagsProp.GetArrayElementAtIndex(0);
                newTagProp.stringValue = newTag;
            }

        }
    }
}
#endif
