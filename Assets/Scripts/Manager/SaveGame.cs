﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveGame
{
    public static void SaveRoomIndex(int roomIndex)
    {
        PlayerPrefs.SetInt("RoomIndex", roomIndex);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Returns the saved room index or 0 if none was found
    /// </summary>
    public static int LoadRoomIndex()
    {
        return PlayerPrefs.GetInt("RoomIndex", 0);
    }

    public static void ResetRoomIndex()
    {
        PlayerPrefs.DeleteKey("RoomIndex");
    }
}