﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneStreamingManager : MonoBehaviour
{

    [SerializeField] Transform defaultPlayerSpawn;
    [SerializeField] RoomFollowCamera followCamera;
    [SerializeField, Range(0, 5)] float playerDeathFadeTime = 2;
    [SerializeField, Scene] List<string> roomScenesOrdered = new List<string>();
    [Space, SerializeField] UnityEvent OnCurrentSceneChanged = new UnityEvent();

    [Header("Editor Scene Loading")]
    [SerializeField, Range(0, 12)] int startSceneIndex = 0;

    static SceneStreamingManager instance;

    List<SceneWithState> scenesOrdered = new List<SceneWithState>();
    int currentSceneIndex;
    bool isReloadingScene;
    Vector3 defaultSpawnPos;

    public static SceneStreamingManager Instance { get => instance; }
    public int CurrentSceneIndex { get => currentSceneIndex; }

    class SceneWithState
    {
        public string sceneName;
        public bool isLoaded;

        public SceneWithState(string sceneName, bool isLoaded = false)
        {
            this.sceneName = sceneName;
            this.isLoaded = isLoaded;
        }
    }

    private void Awake()
    {
        // singleton
        if (instance) Destroy(gameObject);
        else instance = this;

        // set default spawn point
        defaultSpawnPos = defaultPlayerSpawn ? defaultPlayerSpawn.position : Vector3.zero;

        // convert ordered room scenes to ordered room scenes with load state
        for (int i = 0; i < roomScenesOrdered.Count; i++)
            scenesOrdered.Add(new SceneWithState(roomScenesOrdered[i]));

        // update load state if scene is already open to avoid opening a scene twice
        if (SceneManager.sceneCount > 0)
            for (int loadedScenesIdx = 0; loadedScenesIdx < SceneManager.sceneCount; loadedScenesIdx++)
                for (int sceneListIdx = 0; sceneListIdx < scenesOrdered.Count; sceneListIdx++)
                    if (SceneManager.GetSceneAt(loadedScenesIdx).name == scenesOrdered[sceneListIdx].sceneName)
                        scenesOrdered[sceneListIdx].isLoaded = true;
    }

    public void SetCurrentlyPlayedScene(string sceneName)
    {
        int sceneIndex = GetSceneIndexByName(sceneName);
        if (IsSceneIndexValid(sceneIndex))
        {
            currentSceneIndex = sceneIndex;
            OnCurrentSceneChanged?.Invoke();
            SaveGame.SaveRoomIndex(sceneIndex);
            UpdateScenes(sceneIndex);
        }
    }

    public void ReloadCurrentScene(bool isPlayerDead)
    {
        if (!isReloadingScene) StartCoroutine(RespawnAndReloadCurrentScene(isPlayerDead));
    }

    [Button("Load Scene", EButtonEnableMode.Playmode)]
    void DebugLoadScene() { InitialSceneLoad(startSceneIndex); }

    public void InitialSceneLoad(int initialSceneIndex)
    {
        initialSceneIndex = IsSceneIndexValid(initialSceneIndex) ? initialSceneIndex : 0;
        currentSceneIndex = initialSceneIndex;
        OnCurrentSceneChanged?.Invoke();
        LoadSceneAtIndex(initialSceneIndex - 1);
        StartCoroutine(LoadInitialScene(initialSceneIndex));
        LoadSceneAtIndex(initialSceneIndex + 1);
    }

    void UpdateScenes(int playedSceneIndex)
    {
        // only the scenes of the currently played, the one before and the next room should be loaded
        UnloadSceneAtIndex(playedSceneIndex - 2);
        LoadSceneAtIndex(playedSceneIndex + 1);
    }

    AsyncOperation LoadSceneAtIndex(int index)
    {
        if (IsSceneIndexValid(index) && !scenesOrdered[index].isLoaded)
        {
            scenesOrdered[index].isLoaded = true;
            return SceneManager.LoadSceneAsync(scenesOrdered[index].sceneName, LoadSceneMode.Additive);
        }
        return null;
    }

    AsyncOperation UnloadSceneAtIndex(int index)
    {
        if (IsSceneIndexValid(index) && scenesOrdered[index].isLoaded)
        {
            scenesOrdered[index].isLoaded = false;
            return SceneManager.UnloadSceneAsync(scenesOrdered[index].sceneName);
        }
        return null;
    }

    public Scene GetSceneByIndex(int sceneIndex)
    {
        return SceneManager.GetSceneByName(scenesOrdered[sceneIndex].sceneName);
    }

    bool TryGetRoomOfScene(int sceneIndex, out Room room)
    {
        room = null;
        foreach (GameObject go in GetSceneByIndex(sceneIndex).GetRootGameObjects())
            if (go.TryGetComponent(out room)) return true;
        return false;
    }

    int GetSceneIndexByName(string sceneName)
    {
        for (int i = 0; i < scenesOrdered.Count; i++)
            if (scenesOrdered[i].sceneName == sceneName) return i;
        return -1;
    }

    bool IsSceneIndexValid(int index)
    {
        return index >= 0 && index < scenesOrdered.Count;
    }

    Vector3 GetSpawnBySceneIndex(int sceneIndex)
    {
        if (TryGetRoomOfScene(sceneIndex, out Room room) &&
            room.TryGetRespawnPosition(out Vector3 roomSpawn)) return roomSpawn;
        else return defaultSpawnPos;
    }

    IEnumerator RespawnAndReloadCurrentScene(bool isPlayerDead)
    {
        isReloadingScene = true;
        PlayerController player = PlayerController.Instance;
        HUD hud = HUD.Instance;
        hud.SetCanPauseGame(false);

        // reset player transform, hide player
        player.SetVisibility(false);
        player.DetachFromPlatform();
        player.SetCanMove(false);

        // wait until faded to black with death message
        if (isPlayerDead)
        {
            hud.ShowDeathMessageOnBlack(true);
            yield return hud.FadeToBlack(true, playerDeathFadeTime);
        }
        else
        {
            hud.SetToBlack();
            yield return new WaitForSecondsRealtime(0.1f);
        }

        // wait for reload room scene
        UnloadSceneAtIndex(currentSceneIndex);
        AsyncOperation loadingOperation = LoadSceneAtIndex(currentSceneIndex);
        while (!loadingOperation.isDone) yield return null;

        hud.HideInfoText();
        // respawn player
        player.SetVisibility(true);
        player.ResetPlayer();
        player.HealthSystem.RespawnAt(GetSpawnBySceneIndex(currentSceneIndex));
        player.SetCanMove(true);
        yield return null;
        followCamera.FocusTarget();

        // fade in
        hud.ShowDeathMessageOnBlack(false);
        hud.FadeToBlack(false);

        hud.SetCanPauseGame(true);
        isReloadingScene = false;
    }

    IEnumerator LoadInitialScene(int sceneIndex)
    {
        HUD hud = HUD.Instance;
        hud.SetToBlack();
        hud.SetCanPauseGame(false);
        AsyncOperation loadingOperation = LoadSceneAtIndex(sceneIndex);
        while (loadingOperation != null && !loadingOperation.isDone) yield return null;

        PlayerController.Instance.HealthSystem.RespawnAt(GetSpawnBySceneIndex(sceneIndex));
        //yield return null;
        //followCamera?.FocusTarget();
        hud.FadeToBlack(false, 3);
        hud.SetCanPauseGame(true);
    }
}
