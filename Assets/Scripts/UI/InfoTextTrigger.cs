﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class InfoTextTrigger : MonoBehaviour
{
    [SerializeField] bool showOnce = true;
    [SerializeField, ResizableTextArea] string infoTextKeyboard;
    [SerializeField, ResizableTextArea] string infoTextGamepad;
    bool wasShown;

    private void OnTriggerEnter(Collider other)
    {
        if (showOnce && wasShown) return;
        if (other.CompareTag("Player"))
        {
            wasShown = true;
            string text = HUD.Instance.CurrentInputDevice == HUD.InputDevice.Gamepad ? infoTextGamepad : infoTextKeyboard;
            HUD.Instance.ShowInfoText(text);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        HUD.Instance.HideInfoText();
    }

    public void DestroyInfoTextBox()
    {
        HUD.Instance.HideInfoText();
    }
}
