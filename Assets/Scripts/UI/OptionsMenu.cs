﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] AudioMixer mixer;
    [SerializeField] GameObject firstSelected;
    [SerializeField] GameObject selectedWhenClosed;

    UserInputActions input;

    private void Awake()
    {
        input = new UserInputActions();
        input.Enable();
        input.Menu.Cancel.performed += _ => BackToMainMenu();
    }

    void OnEnable() { if (input != null) input.Enable(); }

    void OnDisable() { if (input != null) input.Disable(); }

    public void ShowMenu(bool show)
    {
        if (show)
        {
            gameObject.SetActive(true);
            // clear selected first
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(firstSelected);
        }
        else
        {
            gameObject.SetActive(false);
            // clear selected first
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(selectedWhenClosed);
        }
    }

    public void BackToMainMenu()
    {
        if (gameObject.activeInHierarchy) ShowMenu(false);
    }

    public void SetLevelMusic(float sliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(sliderValue) * 20);
    }

    public void SetLevelSound(float sliderValue)
    {
        mixer.SetFloat("SoundVol", Mathf.Log10(sliderValue) * 20);
    }

    public void SetLevelVoice(float sliderValue)
    {
        mixer.SetFloat("VoiceVol", Mathf.Log10(sliderValue) * 20);
    }
}
