﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;

public class MainMenu : MonoBehaviour
{
    [SerializeField] RoomSelectionMenu roomSelectionMenu;
    [SerializeField] OptionsMenu optionsMenu;
    [SerializeField] GameSceneLoader sceneLoader;

    public void ContinueGame()
    {
        sceneLoader.LoadGameScene(SaveGame.LoadRoomIndex());
    }

    public void NewGame()
    {
        SaveGame.ResetRoomIndex();
        sceneLoader.LoadGameScene(0);
    }

    public void RoomSelectionMenu()
    {
        roomSelectionMenu.ShowMenu(true);
    }

    public void LoadGameAtRoom(int roomIndex)
    {
        sceneLoader.LoadGameScene(roomIndex);
    }

    public void OptionsMenu()
    {
        optionsMenu.ShowMenu(true);
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
