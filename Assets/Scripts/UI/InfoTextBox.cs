﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoTextBox : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textBox;

    public void SetText(string text)
    {
        textBox.SetText(text);
    }
}
