﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using NaughtyAttributes;

public class HUD : MonoBehaviour
{
    [SerializeField] Image healthBar, delayedHealthBar;
    [SerializeField] GameObject effectPreviewOverlay, damageOverlay, deathMessage, gameWonMessage;
    [SerializeField] PauseMenu pauseMenu;
    [SerializeField] CanvasGroup screenFade;
    [SerializeField, Range(0, 5)] float fadeDuration = 0.5f;
    [SerializeField] Canvas canvas;
    [SerializeField] GameObject infoTextBox;

    static HUD instance;
    UserInputActions input;

    InputDevice currentInputDevice;
    bool isGamePaused;
    bool canPauseGame = true;
    GameObject textBoxInstance;

    public enum InputDevice { Keyboard, Gamepad }

    public static HUD Instance { get => instance; }
    public PauseMenu PauseMenu { get => pauseMenu; }
    public Canvas Canvas { get => canvas; }
    public InputDevice CurrentInputDevice { get => currentInputDevice; }
    public bool IsGamePaused { get => isGamePaused; }

    void Awake()
    {
        if (instance) Destroy(gameObject);
        else instance = this;

        // input system
        input = new UserInputActions();
        input.Enable();
        input.Menu.PauseGame.performed += _ => SetGamePaused(!isGamePaused);
        input.Menu.Cancel.performed += _ => CancelGamePaused();
        input.Menu.KeyboardInput.performed += _ => SetCurrentInputDevice(InputDevice.Keyboard);
        input.Menu.GamepadInput.performed += _ => SetCurrentInputDevice(InputDevice.Gamepad);
        input.Menu.Screencapture.performed += _ => CaptureSceen();
    }

    void CaptureSceen()
    {
        ScreenCapture.CaptureScreenshot("ScreenCapture/ScreenCapture_" + Time.time + ".png", 2);
    }

    void SetCurrentInputDevice(InputDevice device)
    {
        if (currentInputDevice != device) currentInputDevice = device;
    }

    void OnEnable() { input?.Enable(); }

    void OnDisable() { input?.Disable(); }

    public void SetGamePaused(bool paused)
    {
        if (!canPauseGame && paused) return;
        isGamePaused = paused;
        Time.timeScale = paused ? 0 : 1;
        AudioListener.pause = paused;
        PauseMenu.ShowMenu(paused);
    }

    void CancelGamePaused()
    {
        if (IsGamePaused) SetGamePaused(false);
    }

    public void SetCanPauseGame(bool canPause)
    {
        canPauseGame = canPause;
        if (!canPause) SetGamePaused(false);
    }

    public void SetHealthBar(float healthNormalized)
    {
        healthBar.fillAmount = healthNormalized;
    }

    public void UpdateDelayedHealthBar(float healthNormalized, float decreaseSpeed)
    {
        StartCoroutine(UpdateHealthBar(healthNormalized, decreaseSpeed));
    }

    public void FlashDamageOverlay(float duration)
    {
        StartCoroutine(ShowDamageOverlayForTime(duration));
    }

    public void ShowDeathMessageOnBlack(bool show)
    {
        AudioManager.instance.PlaySound("Death");
        deathMessage.SetActive(show);
    }

    public void ShowGameWonMessage(bool show)
    {
        gameWonMessage.SetActive(show);
    }

    public Coroutine FadeToBlack(bool fadeOut, float fadeDuration = -1)
    {
        return StartCoroutine(FadeScreen(fadeOut, fadeDuration < 0 ? this.fadeDuration : fadeDuration));
    }

    public void SetToBlack()
    {
        screenFade.alpha = 1;
    }

    public void ShowInfoText(string text)
    {
        Destroy(textBoxInstance);
        textBoxInstance = Instantiate(infoTextBox, canvas.transform);
        textBoxInstance.GetComponent<InfoTextBox>().SetText(text);
    }

    public void HideInfoText()
    {
        Destroy(textBoxInstance);
    }

    IEnumerator FadeScreen(bool fadeOut, float duration)
    {
        float timer = 0;
        while (timer < duration)
        {
            timer += Time.unscaledDeltaTime;
            if (fadeOut) screenFade.alpha = Mathf.Lerp(0, 1, timer / duration);
            else screenFade.alpha = Mathf.Lerp(1, 0, timer / duration);
            yield return null;
        }
    }

    IEnumerator ShowDamageOverlayForTime(float duration)
    {
        damageOverlay.SetActive(true);
        yield return new WaitForSecondsRealtime(duration);
        damageOverlay.SetActive(false);
    }

    IEnumerator UpdateHealthBar(float healthNormalized, float decreaseSpeed)
    {
        float startFill = delayedHealthBar.fillAmount;
        float t = 0;

        while (delayedHealthBar.fillAmount > healthNormalized)
        {
            t += Time.deltaTime;
            delayedHealthBar.fillAmount = Mathf.Lerp(startFill, healthNormalized, t * decreaseSpeed);
            yield return null;
        }
        delayedHealthBar.fillAmount = healthNormalized;
    }
}
