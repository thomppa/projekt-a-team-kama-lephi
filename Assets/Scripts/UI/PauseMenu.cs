﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    [SerializeField, Scene] string mainMenuScene;
    [SerializeField] GameObject firstSelected;

    public void ResumeGame()
    {
        HUD.Instance.SetGamePaused(false);
    }

    public void ReloadRoom()
    {
        HUD.Instance.SetGamePaused(false);
        SceneStreamingManager.Instance.ReloadCurrentScene(false);
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(mainMenuScene);
    }

    public void ShowMenu(bool show)
    {
        if (show)
        {
            gameObject.SetActive(true);
            // clear selected first
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(firstSelected);
        }
        else gameObject.SetActive(false);
    }
}
