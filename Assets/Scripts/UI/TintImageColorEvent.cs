﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TintImageColorEvent : MonoBehaviour
{
    [SerializeField] Color colorTint;

    Image image;
    Color defaultColor;

    void Awake()
    {
        image = GetComponent<Image>();
        defaultColor = image.color;
    }

     public void TintColor(bool tint)
    {
        image.color = tint ? colorTint : defaultColor;
    }
}
