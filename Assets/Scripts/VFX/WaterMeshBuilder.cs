﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[ExecuteInEditMode, RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class WaterMeshBuilder : MonoBehaviour
{
    [SerializeField] float rimThickness = 0.1f;
    [SerializeField, Range(1, 5), OnValueChanged("CreateMesh")] int subdivisions = 2;
    [SerializeField, ReadOnly] int triangleCount;

    Mesh mesh;
    Vector3Int gridSize;
    Vector3 scale;
    Vector3[] vertices;
    int[] triangles;

    void Start()
    {
        CreateMesh();
    }

#if UNITY_EDITOR
    void Update()
    {
        // update when scaling is changed in the editor
        if (!Application.isPlaying && scale != transform.lossyScale) CreateMesh();
    }
#endif

    void CreateMesh()
    {
        // initialize
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        scale = transform.lossyScale;
        gridSize = new Vector3Int(GridSizeFromScale(scale.x), GridSizeFromScale(scale.y), GridSizeFromScale(scale.z)) * subdivisions;

        // place vertices
        vertices = new Vector3[((gridSize.x + 1) * (gridSize.y + 1)) + ((gridSize.x + 1) * (gridSize.z + 1)) + ((gridSize.x + 1) * 3)];

        int i = 0;
        // build front plane
        for (int y = 0; y <= gridSize.y; y++)
        {
            float posY = y / (float)gridSize.y;
            for (int x = 0; x <= gridSize.x; x++)
            {
                float posX = x / (float)gridSize.x;
                vertices[i] = new Vector3(posX, posY, 0);
                i++;
            }
        }

        // build top plane
        for (int z = 0; z <= gridSize.z; z++)
        {
            float posZ = z / (float)gridSize.z;
            for (int x = 0; x <= gridSize.x; x++)
            {
                float posX = x / (float)gridSize.x;
                vertices[i] = new Vector3(posX, 1, posZ);
                i++;
            }
        }

        // build front rim
        float rim = rimThickness / (transform.lossyScale.y);
        for (int x = 0; x <= gridSize.x; x++)
        {
            float posX = x / (float)gridSize.x;
            vertices[i] = new Vector3(posX, 1 - rim, 0);
            i++;
        }
        for (int x = 0; x <= gridSize.x; x++)
        {
            float posX = x / (float)gridSize.x;
            vertices[i] = new Vector3(posX, 1, 0);
            i++;
        }

        // combine vertices to triangles
        triangles = new int[(gridSize.x * gridSize.y * 6) + (gridSize.x * gridSize.z * 6) + (gridSize.x * 3 * 6)];
        int vert = 0;
        int tris = 0;

        for (int y = 0; y < gridSize.y + gridSize.z + 1; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + gridSize.x + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + gridSize.x + 1;
                triangles[tris + 5] = vert + gridSize.x + 2;
                vert++;
                tris += 6;
            }
            vert++;
        }

        vert += gridSize.x + 1;
        for (int x = 0; x < gridSize.x; x++)
        {
            triangles[tris + 0] = vert + 0;
            triangles[tris + 1] = vert + gridSize.x + 1;
            triangles[tris + 2] = vert + 1;
            triangles[tris + 3] = vert + 1;
            triangles[tris + 4] = vert + gridSize.x + 1;
            triangles[tris + 5] = vert + gridSize.x + 2;
            vert++;
            tris += 6;
        }

        // update mesh
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        // color rim verices with red
        Color[] vertexColor = new Color[vertices.Length];
        int waterBodyVertCount = (gridSize.x + 1) * (gridSize.y + 1) + (gridSize.x + 1) * (gridSize.z + 1);
        for (int v = 0; v < waterBodyVertCount + ((gridSize.x + 1) * 2); v++)
            if (v >= waterBodyVertCount) vertexColor[v] = Color.red;
            else vertexColor[v] = Color.black;
        mesh.colors = vertexColor;

        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        triangleCount = triangles.Length / 3;
    }

    int GridSizeFromScale(float scale)
    {
        return Mathf.Clamp(Mathf.Abs(Mathf.RoundToInt(scale)), 1, int.MaxValue);
    }
}
