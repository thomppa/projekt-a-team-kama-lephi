﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VFXHelper : MonoBehaviour
{
    public static VisualEffect SpawnVFX(Vector3 position, VisualEffectAsset vfxAsset, float killDelay = 0, float playRate = 1)
    {
        GameObject go = new GameObject("VFX_" + vfxAsset.name);
        go.transform.position = position;
        go.AddComponent<VisualEffect>().visualEffectAsset = vfxAsset;
        VisualEffect vfx = go.GetComponent<VisualEffect>();
        vfx.playRate = playRate;
        Destroy(go, killDelay);
        return vfx;
    }
}
