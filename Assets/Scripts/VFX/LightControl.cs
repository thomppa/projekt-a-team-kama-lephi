﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControl : PowerActivatable
{
    [SerializeField] bool flickeringLight = false;
    [SerializeField] bool startActive = false;

    public bool lightOn = false;
    public float timeDelay;

    [SerializeField] float minOnRange = 0.01f;
    [SerializeField] float maxOnRange = 0.5f;

    [SerializeField] float minOffRange = 0.01f;
    [SerializeField] float maxOffRange = 0.5f;

    void Start()
    {
        if(startActive)
        {
            lightOn = true;
        }
        else
        {
            lightOn = false;
        }
    }

    void Update()
    {
        if(flickeringLight)
        {
            if (lightOn == false)
            {
                StartCoroutine(FlickeringLight());
            }
        }

        if(startActive)
        {
            if(PowerActive)
            {
                lightOn = false;
            }
            else
            {
                lightOn = true;
            }
        }
        else
        {
            if(PowerActive)
            {
                lightOn = true;
            }
            else
            {
                lightOn = false;
            }
        }

        if(lightOn)
        {
            gameObject.GetComponent<Light>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<Light>().enabled = false;
        }
    }

    IEnumerator FlickeringLight()
    {
        lightOn = true;
        this.gameObject.GetComponent<Light>().enabled = false;
        timeDelay = Random.Range(minOffRange, maxOffRange);
        yield return new WaitForSeconds(timeDelay);
        this.gameObject.GetComponent<Light>().enabled = true;
        timeDelay = Random.Range(minOnRange, maxOnRange);
        yield return new WaitForSeconds(timeDelay);
        lightOn = false;
    }
}
