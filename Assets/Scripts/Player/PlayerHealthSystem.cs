﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(PlayerController))]
public class PlayerHealthSystem : MonoBehaviour
{
    [SerializeField] float maxHealth = 100;
    [SerializeField] float damageCooldown = 0.3f;
    [SerializeField] float damageOverlayFlashDuration = 0.1f;
    [SerializeField] float knockbackStrenght = 1;
    [SerializeField] float minKnockbackMovementCooldown = 0.2f;
    [SerializeField] Transform batteryCharge;
    [SerializeField] Transform batteryChargeWarning;
    [SerializeField] float delayedHealthDecreaseSpeed = 3;

    [Header("Dev Tools")]
    [SerializeField] bool invincible = false;

    PlayerController player;
    bool isAlive;
    float currentHealth;
    bool isDamageOnCooldown;
    float healthUpdateCooldown;

    public float MaxHealth { get => maxHealth; }
    public float CurrentHelth { get => currentHealth; }
    public float MissingHealth { get => maxHealth - currentHealth; }
    public float NormalizedHealth { get => currentHealth / maxHealth; }

    void Start()
    {
        player = GetComponent<PlayerController>();
        // init health with a full heal
        HealFully();
    }

    public void Heal(float value)
    {
        AddToHealth(Mathf.Abs(value));
    }

    public void HealFully()
    {
        AddToHealth(maxHealth);
    }

    /// <param name="isTickDamage">If the damage source is dealing damage each tick.</param>
    public void Damage(float value, bool isTickDamage = false, bool isModuleDamage = false)
    {
        if (isTickDamage && isDamageOnCooldown) return;

        if (isAlive && !isModuleDamage) // give damage feedback if was alive before damage
        {
            AudioManager.instance.PlaySound("DamageTaken");
            HUD.Instance.FlashDamageOverlay(damageOverlayFlashDuration);
        }
        AddToHealth(-Mathf.Abs(value));
        if (isTickDamage) StartCoroutine(DamageCooldown());

    }

    void AddToHealth(float value)
    {
        if (HUD.Instance.IsGamePaused) return;

        currentHealth = Mathf.Clamp(currentHealth + value, 0, maxHealth);
        if (currentHealth <= 0) Kill();
        else isAlive = true;

        // update visuals
        batteryCharge.localScale = new Vector3(1, NormalizedHealth, 1);
        healthUpdateCooldown = damageCooldown * 1.5f;
        StopCoroutine(nameof(HealthUpdateCooldown));
        StartCoroutine(nameof(HealthUpdateCooldown));
        HUD.Instance.SetHealthBar(NormalizedHealth);
    }

    public void Kill()
    {
        if (!isAlive || invincible) return;
        isAlive = false;
        SceneStreamingManager.Instance.ReloadCurrentScene(true);
    }

    public void RespawnAt(Vector3 position)
    {
        HealFully();
        player.Teleport(position);
    }

    public void Knockback(float strenght = 0)
    {
        if (!player.IsClimbing)
        {
            StartCoroutine(CanMoveOnGrounded());
            if (strenght <= 0) strenght = knockbackStrenght;
            Vector3 velocity = player.InputVelocity.normalized;
            player.SetVelocity(new Vector3(strenght * -Mathf.Sign(velocity.x), strenght * -Mathf.Sign(velocity.y), 0));
        }
    }

    IEnumerator DamageCooldown()
    {
        isDamageOnCooldown = true;
        yield return new WaitForSeconds(damageCooldown);
        isDamageOnCooldown = false;
    }

    IEnumerator CanMoveOnGrounded()
    {
        player.SetCanMove(false);
        yield return new WaitForSeconds(minKnockbackMovementCooldown);
        while (!player.IsGrounded && !player.IsWindOrWaterInteracting) yield return null;
        player.SetCanMove(true);
    }

    IEnumerator HealthUpdateCooldown()
    {
        yield return new WaitForSeconds(damageCooldown * 1.5f);
        StartCoroutine(UpdateBatteryCharge());
        HUD.Instance.UpdateDelayedHealthBar(NormalizedHealth, delayedHealthDecreaseSpeed);
    }

    IEnumerator UpdateBatteryCharge()
    {
        float startScale = batteryChargeWarning.localScale.y;
        float t = 0;

        if (batteryChargeWarning.localScale.y > NormalizedHealth)
            while (batteryChargeWarning.localScale.y > NormalizedHealth)
            {
                t += Time.deltaTime;
                float charge = Mathf.Lerp(startScale, NormalizedHealth, t * delayedHealthDecreaseSpeed);
                batteryChargeWarning.localScale = new Vector3(1, charge, 1);
                yield return null;
            }
        batteryChargeWarning.localScale = new Vector3(1, NormalizedHealth, 1);
    }
}
