﻿using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerShaderInteraction : MonoBehaviour
{
    [SerializeField] float velocitySmoothing = 10;

    PlayerController player;
    Vector2 velocity;

    void Awake()
    {
        player = GetComponent<PlayerController>();
    }

    void Update()
    {
        // set player position
        Shader.SetGlobalVector("playerPosition", transform.position);

        // smooth player velocity for shader
        float deltaSmooth = velocitySmoothing * Time.deltaTime;
        Vector2 newVelocity = new Vector2(
            Mathf.Lerp(velocity.x, player.GetSwimVelocityNegOneToOne().x, deltaSmooth),
            Mathf.Lerp(velocity.y, player.GetSwimVelocityNegOneToOne().y, deltaSmooth));
        velocity = newVelocity;
        Shader.SetGlobalVector("playerVelocityNormalized", newVelocity);
    }
}
