﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PlayerUI : MonoBehaviour
{
    [SerializeField] Material interactionProgressMat;
    [SerializeField] DeviceUIObject interactionProgressUI;
    [SerializeField] DeviceUIObject interactionButton;
    [SerializeField] DeviceUIObject climbButton;

    [HideInInspector] public float interactionProgress;

    PlayerController player;
    DeviceUIObject currentOverheadUI;
    GameObject currentOverheadUIObject;

    void Start()
    {
        player = GetComponent<PlayerController>();
    }

    void Update()
    {
        currentOverheadUI = null;

        if (player.CouldAttachToClimbable) currentOverheadUI = climbButton;

        if (player.IsModuleAvailable && !player.IsInteractingWithModule) currentOverheadUI = interactionButton;

        if (player.IsInteractingWithModule)
        {
            interactionProgressMat.SetFloat("progress", interactionProgress);
            currentOverheadUI = interactionProgressUI;
        }

        UpdateOverheadUI();
    }

    void UpdateOverheadUI()
    {
        if (currentOverheadUI == null)
        {
            if (currentOverheadUIObject) currentOverheadUIObject.SetActive(false);
            currentOverheadUIObject = null;
        }
        else
        {
            GameObject newUIObject = HUD.Instance.CurrentInputDevice == HUD.InputDevice.Gamepad ? currentOverheadUI.gamepad : currentOverheadUI.keyboard;
            if (newUIObject != currentOverheadUIObject)
            {
                if (currentOverheadUIObject) currentOverheadUIObject.SetActive(false);
                newUIObject.SetActive(true);
                currentOverheadUIObject = newUIObject;
            }
        }
    }
}

[System.Serializable]
public class DeviceUIObject
{
    public GameObject keyboard;
    public GameObject gamepad;

    public DeviceUIObject(GameObject keyboardUI, GameObject gamepadUI)
    {
        keyboard = keyboardUI;
        gamepad = gamepadUI;
    }
}
