﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(PlayerController))]
public class PlayerAnimationFX : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] Animator animator;
    [SerializeField] float moveSpeedSmoothing = 10;
    [SerializeField] float facingRotationSpeed = 30;
    [SerializeField] float swimRotationSpeed = 10;
    [SerializeField, Range(0, 1)] float climbTransitionTime = 0.2f;
    [SerializeField] bool defaultFacingLeft = false;
    [SerializeField] float minIdleWaitTime = 20;
    [SerializeField] float groundCheckDistance = 1;
    [SerializeField] LayerMask groundCheckLayer;
    [SerializeField] Transform playerRoot;

    [Header("VFX")]
    [SerializeField] VisualEffect waterBubbles;
    [SerializeField, Range(0.5f, 2)] float waterBubblesRate = 1;
    [SerializeField] VisualEffect waterSplashes;
    [SerializeField, Range(0.5f, 2)] float waterSplashesRate = 1;
    [SerializeField] VisualEffectAsset waterBurst;
    [SerializeField, Range(0.5f, 2)] float waterBurstRate = 1;
    [SerializeField] ParticleSystem climbAttach;

    PlayerController player;
    bool isAtemptingIdleWait;
    bool isClimbTransitioning;
    float facingRotationTarget;

    bool CanIdleWait { get => player.Input.magnitude == 0 && player.IsGrounded && !player.IsClimbing && !player.IsSwimming && !player.IsInteractingWithModule; }

    void Start()
    {
        player = GetComponent<PlayerController>();
    }

    void Update()
    {
        UpdateAnimator();
        UpdateFacing();
    }

    // Animation Methods
    void UpdateAnimator()
    {
        if (EarlyGroundCheck() && !player.IsGrounded) animator.SetBool("isFalling", false);
        else animator.SetBool("isFalling", !player.IsGrounded);

        animator.SetBool("isSwimming", player.IsSwimming);
        animator.SetBool("isClimbing", player.IsClimbing);
        animator.SetBool("isActivatingModule", player.IsInteractingWithModule);

        // set animatior moveSpeed to player input if they are actually moving
        animator.SetFloat("moveSpeedHorizontal", Mathf.Abs(player.Velocity.x));

        // stop animation if climbing but not moving
        animator.speed = !isClimbTransitioning && player.IsClimbing && player.Velocity.magnitude == 0 ? 0 : 1;
        animator.SetFloat("moveSpeedMagnitude", player.Velocity.magnitude);

        // try to start push up idle animation timer
        if (CanIdleWait && !isAtemptingIdleWait) StartCoroutine(IdleWaiting());
    }

    // VFX Methods
    public void SetWaterSurfacePosition(float surfacePositionY)
    {
        waterBubbles.SetFloat("WaterSurfacePositionY", surfacePositionY + 0.3f);
    }

    public void SetWaterBubblesActive(bool active)
    {
        if (active)
        {
            waterBubbles.Play();
            waterBubbles.playRate = waterBubblesRate;
        }
        else waterBubbles.Stop();
    }

    public void UpdateWaterBubbles(float swimVelocityNormalized)
    {
        float bubbleAmount = 0.2f;
        if (swimVelocityNormalized > bubbleAmount) bubbleAmount = Mathf.Clamp01(swimVelocityNormalized);
        waterBubbles.SetFloat("PlayerVelocityNormalized", bubbleAmount);
    }

    public void SetWaterSplashesActive(bool active)
    {
        if (active)
        {
            waterSplashes.Play();
            waterBubbles.playRate = waterSplashesRate;
        }
        else waterSplashes.Stop();
    }

    public void UpdateWaterSplashes(float swimVelocityNormalized, float surfacePositionY, bool updatePosition = true)
    {
        if (updatePosition) waterSplashes.transform.position = new Vector3(transform.position.x, surfacePositionY + 0.3f, transform.position.z);
        if (swimVelocityNormalized > 0.1f) waterSplashes.SetFloat("PlayerVelocityNormalized", swimVelocityNormalized);
        else waterSplashes.SetFloat("PlayerVelocityNormalized", 0);
    }

    public void SpawnWaterBurst(Vector3 spawnPosition)
    {
        VFXHelper.SpawnVFX(spawnPosition, waterBurst, 3, waterBurstRate);
    }

    public void EnterClimbing()
    {
        climbAttach.Play();
        StopCoroutine(nameof(ClimbingAnimatorTransition));
        StartCoroutine(nameof(ClimbingAnimatorTransition));
    }

    void UpdateFacing()
    {
        // facing when climbing
        if (player.IsClimbing)
        {
            playerRoot.localRotation = Quaternion.Euler(0, -90, 0);
            return;
        }
        else
        {
            // facing when swimming
            float smoothedAngleZ;
            if (player.IsSwimming)
            {
                float inputAngle = Mathf.Clamp(Vector2.Angle(Vector2.up, player.Input) - 20, 0, 180);
                smoothedAngleZ = Mathf.MoveTowardsAngle(playerRoot.localEulerAngles.z, defaultFacingLeft ? inputAngle : -inputAngle, swimRotationSpeed);
            }
            else smoothedAngleZ = 0;

            // facing when walking
            if (player.Velocity.x != 0 && player.CanMove)
            {
                // determine the facing direction by actual velocity if no input is given
                float dirVelocity = player.Input.x != 0 ? player.Input.x : player.Velocity.x;
                facingRotationTarget = dirVelocity < 0 != defaultFacingLeft ? 180 : 0;
            }
            float smoothedAngleY = Mathf.MoveTowards(playerRoot.localEulerAngles.y, facingRotationTarget, facingRotationSpeed);

            playerRoot.localRotation = Quaternion.Euler(0, smoothedAngleY, smoothedAngleZ);
        }
    }

    bool EarlyGroundCheck()
    {
        return Physics.Raycast(transform.position, Vector3.down, player.Controller.height / 2 + groundCheckDistance, groundCheckLayer);
    }

    IEnumerator ClimbingAnimatorTransition()
    {
        isClimbTransitioning = true;
        yield return new WaitForSeconds(climbTransitionTime);
        isClimbTransitioning = false;
    }

    IEnumerator IdleWaiting()
    {
        isAtemptingIdleWait = true;
        // wait initial time
        float waitTime = 0;
        while (waitTime < minIdleWaitTime)
        {
            if (!CanIdleWait)
            {
                isAtemptingIdleWait = false;
                yield break;
            }
            waitTime += Time.deltaTime;
            yield return null;
        }
        // push ups while can idle
        animator.SetBool("isIdleWaiting", true);
        while (CanIdleWait) yield return null;
        // cancel push ups
        animator.SetBool("isIdleWaiting", false);
        isAtemptingIdleWait = false;
    }
}
