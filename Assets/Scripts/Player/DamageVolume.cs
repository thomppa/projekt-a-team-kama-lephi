﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(Collider))]
public class DamageVolume : MonoBehaviour
{
    [SerializeField] bool knockBack = false;
    [SerializeField] bool tickDamage = false;
    [SerializeField] float damage = 10;

    bool isActive = true;

    public float Damage { get => damage; set => damage = value; }
    public bool IsActive { get => isActive; set => isActive = value; }

    void OnTriggerEnter(Collider other)
    {
        if (isActive && other.CompareTag("Player") && other.TryGetComponent(out PlayerHealthSystem health))
        {
            if (!tickDamage) health.Damage(damage, false);
            if (knockBack) health.Knockback();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (isActive && tickDamage && other.CompareTag("Player") && other.TryGetComponent(out PlayerHealthSystem health))
            health.Damage(damage, true);
    }
}
