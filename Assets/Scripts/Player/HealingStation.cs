﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class HealingStation : MonoBehaviour
{
    [SerializeField] bool healOnce = false;
    [SerializeField] float healthPool = 100;
    [SerializeField, Foldout("Advanced Properties")] float maxHealthPool = 200;
    [SerializeField, Foldout("Advanced Properties")] Transform healthPoolTrans;

    [ReadOnly, SerializeField] float currentHealthPool;
    bool healedOnce = false;

    void Start()
    {
        UpdateHealthPool(healthPool);
    }

    void OnTriggerEnter(Collider other)
    {
        if (healOnce && healedOnce) return;

        if (other.CompareTag("Player") && other.TryGetComponent(out PlayerHealthSystem health))
        {
            float healAmount = currentHealthPool < health.MissingHealth ? currentHealthPool : health.MissingHealth;

            if (healOnce && health.MissingHealth > 0)
            {
                healedOnce = true;
                UpdateHealthPool(0);
            }
            else UpdateHealthPool(currentHealthPool - healAmount);

            health.Heal(healAmount);
        }
    }

    void UpdateHealthPool(float healthPool)
    {
        AudioManager.instance.PlaySound("BatterieRefill");
        currentHealthPool = Mathf.Clamp(healthPool, 0, float.MaxValue);
        healthPoolTrans.localScale = new Vector3(1, currentHealthPool / maxHealthPool, 1);

        healthPoolTrans.gameObject.SetActive(currentHealthPool > 0);
    }
}
