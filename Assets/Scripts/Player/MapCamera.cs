﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MapCamera : MonoBehaviour
{
    [SerializeField] float activationDuration = 7;
    [SerializeField] float deactivationDuration = 1;

    Camera thisCamera;
    float mapViewFOV;
    Vector3 mapViewPosition;

    Vector3 inPosition;
    Vector3 outPosition;
    float inFOV;
    float outFOV;

    bool isActivating;
    bool isDeactivating;
    float transitionTime;
    float maxTransitionTime;

    void Awake()
    {
        thisCamera = GetComponent<Camera>();
        thisCamera.enabled = false;
        mapViewFOV = thisCamera.fieldOfView;
        mapViewPosition = transform.position;
    }

    void Update()
    {
        if (isActivating || isDeactivating)
        {
            transitionTime += Time.deltaTime;
            transform.position = Vector3.Lerp(inPosition, outPosition, transitionTime / maxTransitionTime);
            thisCamera.fieldOfView = Mathf.Lerp(inFOV, outFOV, transitionTime / maxTransitionTime);
        }
    }

    public bool TryActivate(Camera followCamera)
    {
        if (isActivating || isDeactivating) return false;

        followCamera.enabled = false;
        thisCamera.enabled = true;

        outPosition = mapViewPosition;
        outFOV = mapViewFOV;

        inPosition = followCamera.transform.position;
        inFOV = followCamera.fieldOfView;

        StartCoroutine(CameraTransitionActivate());
        return true;
    }

    public bool TryDeactivate(Camera followCamera)
    {
        if (isDeactivating) return false;

        inPosition = transform.position;
        inFOV = thisCamera.fieldOfView;

        outPosition = followCamera.transform.position;
        outFOV = followCamera.fieldOfView;

        StopAllCoroutines();
        isActivating = false;
        StartCoroutine(CameraTransitionDeactivate(followCamera));
        return true;
    }

    IEnumerator CameraTransitionActivate()
    {
        isActivating = true;
        transitionTime = 0;
        maxTransitionTime = activationDuration;
        yield return new WaitForSeconds(maxTransitionTime);
        isActivating = false;
    }

    IEnumerator CameraTransitionDeactivate(Camera followCamera)
    {
        isDeactivating = true;
        transitionTime = 0;
        maxTransitionTime = deactivationDuration;
        yield return new WaitForSeconds(maxTransitionTime);
        thisCamera.enabled = false;
        followCamera.enabled = true;
        isDeactivating = false;
    }
}
