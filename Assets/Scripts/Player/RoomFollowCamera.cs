﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[RequireComponent(typeof(Camera))]
public class RoomFollowCamera : MonoBehaviour
{
    [Tooltip("higher number = later camera follow \n0 = constant follow")]
    [SerializeField, Range(0, 30)] float smoothingExponent = 5;
    [Tooltip("The distance at which the camera starts to slow down before hittin the bounds of the room.")]
    [SerializeField, Range(0, 10)] float boundsSlowDistance = 3;
    [SerializeField] Vector3 cameraOffset = new Vector3(0, 0, -10);
    [Space]
    [SerializeField] float roomTransitionTime = 3;

    Camera thisCamera;
    Transform player;
    Room currentRoom;

    bool isTransitioningRooms;
    float transitionTime;
    Vector3 transitionStartCameraPos;

    public Camera Camera { get => thisCamera; }

    void Start()
    {
        thisCamera = GetComponent<Camera>();
        player = PlayerController.Instance.transform;
    }

    void Update()
    {
        if (!currentRoom) return;

        if (isTransitioningRooms)
        {
            transitionTime += Time.deltaTime;
            transform.position = Vector3.Lerp(transitionStartCameraPos, ClampCameraPosition(GetCameraTargetPosition()), transitionTime / roomTransitionTime);
        }
        else CameraFollowTarget();
    }

    public void ChangeRooms(Room newRoom)
    {
        if (newRoom && currentRoom != newRoom)
        {
            // reset camera position if this is the first room entered
            if (!currentRoom && newRoom)
            {
                currentRoom = newRoom;
                FocusTarget();
            }

            currentRoom = newRoom;
            StopAllCoroutines();
            StartCoroutine(RoomTransition());
        }
    }

    public void FocusTarget()
    {
        isTransitioningRooms = false;
        transform.position = ClampCameraPosition(GetCameraTargetPosition());
    }

    void CameraFollowTarget()
    {
        Vector3 targetCameraPos = new Vector3(
            Mathf.Lerp(transform.position.x, GetCameraTargetPosition().x, AxisSmoothSpeed(true)),
            Mathf.Lerp(transform.position.y, GetCameraTargetPosition().y, AxisSmoothSpeed(false)),
            GetCameraTargetPosition().z);

        transform.position = ClampCameraPosition(targetCameraPos);
    }

    float AxisSmoothSpeed(bool isXAxis)
    {
        // get viewport position of target. the value inreases when getting closer to screen border 
        Vector2 targetViewportPos = thisCamera.ScreenToViewportPoint(thisCamera.WorldToScreenPoint(player.position));

        // get current smoothing speed by using the viewportPos in an exponetial function
        float x = Mathf.Abs((isXAxis ? targetViewportPos.x : targetViewportPos.y) - 0.5f);
        float smoothSpeed = Mathf.Exp(smoothingExponent * 2 * x - smoothingExponent); // f() = e^(2a*x-a) + m
        float deltaSmoothSpeed = Mathf.Clamp01(smoothSpeed * 50 * Time.deltaTime);

        // lower the smoothSpeed when near a wall and moving towards it
        if (isXAxis)
        {
            if (GetDistanceToRoomBounds(GetCameraTargetPosition()).x < GetDistanceToRoomBounds(transform.position).x)
                return Mathf.Lerp(0, deltaSmoothSpeed, GetDistanceToRoomBounds(transform.position).x / boundsSlowDistance);
        }
        else
        {
            if (GetDistanceToRoomBounds(GetCameraTargetPosition()).y < GetDistanceToRoomBounds(transform.position).y)
                return Mathf.Lerp(0, deltaSmoothSpeed, GetDistanceToRoomBounds(transform.position).y / (boundsSlowDistance / 1.5f));
        }
        return deltaSmoothSpeed;
    }

    Vector3 GetCameraTargetPosition()
    {
        float targetZ = currentRoom ? currentRoom.transform.position.z : player.transform.position.z;
        return new Vector3(player.transform.position.x, player.transform.position.y, targetZ) + cameraOffset;
    }

    Vector3 ClampCameraPosition(Vector3 targetCameraPos)
    {
        Vector2 roomHalfSize = currentRoom.Size / 2 - GetViewportHalfSizeAtZPlane(currentRoom.transform.position.z);
        Vector3 roomOrigin = currentRoom.transform.position;

        // if camera screen is larger than room, position camera at room center
        if (roomHalfSize.x < 0 || roomHalfSize.y < 0) return new Vector3(roomOrigin.x, roomOrigin.y, targetCameraPos.z);
        else
        {
            float cameraPosX = Mathf.Clamp(targetCameraPos.x, roomOrigin.x - roomHalfSize.x, roomOrigin.x + roomHalfSize.x);
            float cameraPosY = Mathf.Clamp(targetCameraPos.y, roomOrigin.y - roomHalfSize.y, roomOrigin.y + roomHalfSize.y);
            return new Vector3(cameraPosX, cameraPosY, targetCameraPos.z);
        }
    }

    Vector2 GetDistanceToRoomBounds(Vector3 point)
    {
        Vector2 roomHalfSize = currentRoom.Size / 2 - GetViewportHalfSizeAtZPlane(currentRoom.transform.position.z);
        Vector3 distanceToOrigin = currentRoom.transform.position - point;
        return new Vector2(
            Mathf.Clamp(roomHalfSize.x - Mathf.Abs(distanceToOrigin.x), 0, float.MaxValue),
            Mathf.Clamp(roomHalfSize.y - Mathf.Abs(distanceToOrigin.y), 0, float.MaxValue));
    }

    Vector2 GetViewportHalfSizeAtZPlane(float zPlanePos)
    {
        float adj = Mathf.Abs(zPlanePos - transform.position.z);
        // adjacentSide * tan(angle) = oppositeSide
        float halfSizeY = adj * Mathf.Tan((thisCamera.fieldOfView / 2) * Mathf.Deg2Rad);

        return new Vector2(halfSizeY * thisCamera.aspect, halfSizeY);
    }

    IEnumerator RoomTransition()
    {
        transitionTime = 0;
        transitionStartCameraPos = transform.position;
        isTransitioningRooms = true;
        yield return new WaitForSeconds(roomTransitionTime);
        isTransitioningRooms = false;
    }
}
