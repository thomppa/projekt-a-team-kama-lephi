﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController), typeof(PlayerAnimationFX), typeof(PlayerHealthSystem))]
public class PlayerController : MonoBehaviour
{
    [Header("Ground Movement")]
    [SerializeField] float maxGroundSpeed = 6;
    [SerializeField] float groundAcceleration = 80;
    [SerializeField] float groundDeceleration = 80;
    [Space]
    [SerializeField] float maxAirSpeed = 6;
    [SerializeField] float airAcceleration = 20;
    [SerializeField] float airDeceleration = 7;
    [Space]
    [SerializeField] float jumpHeight = 2;
    [SerializeField] float minGravity = 4;
    [SerializeField] float gravity = 9.81f;

    [Header("Wind Movement")]
    [SerializeField] float windUpSpeed = 4;
    [SerializeField] float windSidewaysSpeed = 4;
    [SerializeField] float windUpAcceleration = 50;
    [SerializeField] float windEnterCooldown = 0.1f;

    [Header("Water Movement")]
    [SerializeField] float maxSwimSpeed = 3;
    [SerializeField] float swimAcceleration = 6;
    [SerializeField] float swimDeceleration = 2;
    [Tooltip("When entering water, the velocity gets clamped to a max of MaxSwimSpeed multipied my this amount.")]
    [SerializeField, Range(1, 3)] float enterWaterMaxSpeedMult = 2;
    [SerializeField] float enterWaterCooldown = 0.2f;
    [Space]
    [Tooltip("The distance between the players center and the water surface, where the player still floats to the top.")]
    [SerializeField] float maxSurfaceFloatDistance = 1.5f;
    [SerializeField] float floatUpSpeed = 1.2f;
    [SerializeField] float sinkDownSpeed = 0.7f;

    [Header("Plant Movement")]
    [SerializeField] float maxClimbSpeed = 2;
    [SerializeField] float climbAcceleration = 50;
    [SerializeField] float climbDeceleration = 50;
    [SerializeField, Range(0, 1)] float climbWindScale = 0.3f;
    [Space]
    [SerializeField] float jumpTravelTime = 0.2f;
    [SerializeField] float minJumpDistance = 1;
    [SerializeField] float maxJumpDistance = 3;
    [SerializeField] float minJumpChargeTime = 0.1f;
    [SerializeField] float maxJumpChargeTime = 1;
    [SerializeField, Range(0, 0.3f)] float spriteRimThickness = 0.07f;

    [Header("Other")]
    [SerializeField] float moduleActivationTime = 1;
    [SerializeField] float effectSurfaceTolerance = 0.2f;

    [Header("Advanced")]
    [SerializeField] float defaultZPosition = 2;
    [SerializeField, Required] RoomFollowCamera followCamera;
    [SerializeField] Transform characterRoot;
    [SerializeField] GameObject playerVisuals;
    [SerializeField] GameObject climbJumpArrow;
    [SerializeField] GameObject climbJumpCircle;
    [SerializeField] float capsuleEndCheckOffset = 0.05f;
    [SerializeField] float capsuleSidesCheckOffset = 0.03f;
    [SerializeField] LayerMask groundLayer;

    [Space(10), ReadOnly, SerializeField] Vector3 velocity;

    static PlayerController instance;
    PlayerHealthSystem healthSystem;
    UserInputActions input;
    CharacterController controller;
    PlayerAnimationFX playerFX;

    float inputHorizontal;
    float inputVertical;

    bool canMove = true;
    bool isGrounded;
    bool isCeilinged;
    bool isVisible = true;
    float currentMaxSpeed;

    HashSet<EffectVolumeBase> activeEffects = new HashSet<EffectVolumeBase>();
    bool isEffectJumping;
    bool isWindActive;
    WindDirection activeWindDirection;
    EffectVolumeBase activeWindEffect;
    bool isWaterActive;
    EffectVolumeBase activeWaterEffect;
    bool showEffectPreviews;

    HashSet<GameObject> availableClimbables = new HashSet<GameObject>();
    bool isClimbing;

    bool jumpKeyWasPressed;
    bool chargingClimbJump;
    bool performingClimbJump;
    float climbJumpChargeTime;

    Room currentRoom;
    Module currentModule;
    bool isInteractingWithModule;

    public enum MovementMode { Walking, Pushing, Floating, Swimming, Climbing }
    public static PlayerController Instance { get => instance; }
    public PlayerHealthSystem HealthSystem { get => healthSystem; }
    public Vector2 Input { get => new Vector2(inputHorizontal, inputVertical); }
    public Vector3 InputVelocity { get => velocity; }
    public Vector3 Velocity { get => new Vector3(controller.velocity.x / currentMaxSpeed, controller.velocity.y / currentMaxSpeed, 0); }
    public bool IsCeilinged { get => isCeilinged; }
    public bool IsGrounded { get => isGrounded; }
    public bool CanMove { get => canMove; }
    public CharacterController Controller { get => controller; }
    public bool IsSwimming { get => isWaterActive; }
    public bool IsClimbing { get => isClimbing; }
    public bool IsWindOrWaterInteracting { get => isWindActive || isWaterActive; }
    public bool IsModuleAvailable { get => currentModule != null; }
    public bool IsInteractingWithModule { get => isInteractingWithModule; }
    public bool CouldAttachToClimbable { get => availableClimbables.Count > 0 && !isClimbing && !performingClimbJump; }

    void Awake()
    {
        if (!instance) instance = this;
        else
        {
            Debug.LogError("Player singleton already exists!");
            Destroy(gameObject);
        }

        healthSystem = GetComponent<PlayerHealthSystem>();
        playerFX = GetComponent<PlayerAnimationFX>();
        controller = GetComponent<CharacterController>();

        InitializeInputs();
    }

    void Update()
    {
        if (HUD.Instance.IsGamePaused || !isVisible) return;

        // update player movement
        Collider[] ceilingCollider = CheckCapsuleEndCollision(out isCeilinged, false);
        Collider[] groundCollider = CheckCapsuleEndCollision(out isGrounded, true);
        Collider[] sidesCollider = CheckCapsuleSidesCollision();

        HandleWallCollision(sidesCollider);
        HandleMovingPlatformAttachment(groundCollider);
        if (CheckIfPlayerCrushed(ceilingCollider, groundCollider))
        {
            healthSystem.Damage(healthSystem.MaxHealth);
            healthSystem.Kill();
        }

        UpdateMovement();
        UpdateEffectPreview();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Room")) TryChangeRooms(other.GetComponent<Room>());

        if (other.CompareTag("Module")) currentModule = other.GetComponent<Module>();

        if (other.gameObject.layer == LayerMask.NameToLayer("Effect")) activeEffects.Add(other.GetComponent<EffectVolumeBase>());

        if (other.CompareTag("Climbable")) availableClimbables.Add(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Module") && currentModule == other.GetComponent<Module>()) currentModule = null;

        if (other.gameObject.layer == LayerMask.NameToLayer("Effect")) activeEffects.Remove(other.GetComponent<EffectVolumeBase>());

        if (other.CompareTag("Climbable"))
        {
            availableClimbables.Remove(other.gameObject);
            if (availableClimbables.Count == 0) isClimbing = false;
        }
    }

    void OnEnable() { if (input != null) input.Enable(); }

    void OnDisable() { if (input != null) input.Disable(); }

    void InitializeInputs()
    {
        input = new UserInputActions();
        input.Enable();
        // subscribe to input
        input.Player.Jump.performed += _ => InputJump();
        input.Player.Climb.performed += _ => InputToggleClimbing();
        input.Player.EffectPreview.started += _ => InputShowEffectPreview(true);
        input.Player.EffectPreview.canceled += _ => InputShowEffectPreview(false);
        input.Player.EffectPreviewToggle.performed += _ => InputShowEffectPreviewToggle();
        input.Player.Interact.started += _ => InputModuleInteraction(true);
        input.Player.Interact.canceled += _ => InputModuleInteraction(false);
    }

    public void SetVisibility(bool isVisible)
    {
        playerVisuals.SetActive(isVisible);
        this.isVisible = isVisible;
    }

    public void ResetPlayer()
    {
        availableClimbables.Clear();
        activeEffects.Clear();
        currentModule = null;
        DetachFromPlatform();
    }

    void UpdateMovement()
    {
        EvaluateActiveEffects();
        UpdateAxisInput();

        if (!performingClimbJump)
        {
            if (isClimbing)
            {
                float windUp = isWindActive && activeWindDirection == WindDirection.Left ? windUpSpeed : 0;
                ClimbingMovement(!chargingClimbJump, GetCurrentSidewaysWindSpeed() * climbWindScale, windUp * climbWindScale);
                ChargeClimbJump();
                currentMaxSpeed = maxClimbSpeed;
            }
            else if (isWaterActive)
            {
                SwimmingMovement();
                currentMaxSpeed = maxSwimSpeed;
            }
            else
            {
                WalkingMovement(GetCurrentSidewaysWindSpeed());

                if (!isEffectJumping && !chargingClimbJump && isWindActive && activeWindDirection == WindDirection.Up)
                    WindUpMovement();
                else UpdateGravity();
            }
        }

        // resetting z position if changed
        if (transform.position.z != defaultZPosition)
        {
            Vector3 pos = transform.position;
            if (controller.velocity.z != 0) pos.x -= 0.01f * Mathf.Sign(velocity.x);
            transform.position = new Vector3(pos.x, pos.y, defaultZPosition);
        }
        controller.Move(velocity * Time.deltaTime);
    }

    void UpdateAxisInput()
    {
        if (canMove)
        {
            inputHorizontal = input.Player.MoveHorizontal.ReadValue<float>();
            inputVertical = input.Player.MoveVertical.ReadValue<float>();
        }
        else inputHorizontal = inputVertical = 0;
    }

    void UpdateGravity()
    {
        if (isCeilinged && velocity.y > 0) velocity.y = 0;
        if (isGrounded && velocity.y < 0) velocity.y = -minGravity;
        velocity.y = Mathf.Clamp(velocity.y - gravity * Time.deltaTime, -50, 50);
    }

    void WalkingMovement(float addForceX = 0)
    {
        float desiredSpeed;
        float acceleration;

        if (isGrounded)
        {
            desiredSpeed = inputHorizontal * maxGroundSpeed + addForceX;
            acceleration = inputHorizontal != 0 ? groundAcceleration : groundDeceleration;
            currentMaxSpeed = maxGroundSpeed;
        }
        else
        {
            desiredSpeed = inputHorizontal * maxAirSpeed + addForceX;
            acceleration = inputHorizontal != 0 ? airAcceleration : airDeceleration;
            currentMaxSpeed = maxAirSpeed;
        }

        velocity.x = Mathf.MoveTowards(velocity.x, desiredSpeed, acceleration * Time.deltaTime);
    }

    void WindUpMovement()
    {
        currentMaxSpeed = maxAirSpeed;
        if (IsOnSurface(activeWindEffect)) velocity.y = 0;
        else velocity.y = Mathf.MoveTowards(velocity.y, windUpSpeed, windUpAcceleration * Time.deltaTime);
    }

    void SwimmingMovement(float addForceX = 0)
    {
        float desiredSpeedX = 0;
        float desiredSpeedY = 0;

        if (!TryGetTransformSurfacePosY(activeWaterEffect, out float waterSurfacePosY))
            return;

        if ((isCeilinged && velocity.y > 0) || (isGrounded && velocity.y < 0))
            velocity.y = 0;

        if (IsOnSurface(activeWaterEffect) && !isEffectJumping)
        {
            velocity.y = 0;
            transform.position = new Vector3(transform.position.x, waterSurfacePosY, transform.position.z);
        }

        // check if any input is given
        if (inputHorizontal == 0 && inputVertical == 0)
        {
            // Check if floating to surface is valid
            if (waterSurfacePosY > transform.position.y && waterSurfacePosY - transform.position.y < maxSurfaceFloatDistance)
            {
                desiredSpeedY = floatUpSpeed;
                Debug.DrawRay(transform.position + -transform.forward / 2, Vector3.up * (waterSurfacePosY - transform.position.y), Color.green);
            }
            // Otherwise pull player down
            else
            {
                Debug.DrawRay(transform.position - transform.forward / 2, Vector3.up * (waterSurfacePosY - transform.position.y), Color.red);
                desiredSpeedY = -sinkDownSpeed;
            }
        }
        else if (waterSurfacePosY + 0.1f > transform.position.y)
        {
            desiredSpeedX = Input.normalized.x * maxSwimSpeed + addForceX;
            desiredSpeedY = Input.normalized.y * maxSwimSpeed;
        }

        // Horizontal swimming movement
        float accelerationX = inputHorizontal != 0 ? swimAcceleration : swimDeceleration;
        velocity.x = Mathf.MoveTowards(velocity.x, desiredSpeedX, accelerationX * Time.deltaTime);

        // Vertical swimming movement
        float accelerationY = inputVertical != 0 ? swimAcceleration : swimDeceleration;
        velocity.y = Mathf.MoveTowards(velocity.y, desiredSpeedY, accelerationY * Time.deltaTime);

        // update VFX
        playerFX.SetWaterSurfacePosition(waterSurfacePosY);
        playerFX.UpdateWaterBubbles(GetSwimVelocityNegOneToOne().magnitude);

        if (IsOnSurface(activeWaterEffect, effectSurfaceTolerance))
            playerFX.UpdateWaterSplashes(GetSwimVelocityNegOneToOne().magnitude, waterSurfacePosY);
        else playerFX.UpdateWaterSplashes(0, waterSurfacePosY, false);
    }

    void ClimbingMovement(bool useInput, float addForceX = 0, float addForceY = 0)
    {
        float desiredSpeedX = useInput ? Input.normalized.x * maxClimbSpeed + addForceX : 0;
        float accelerationX = useInput && inputHorizontal != 0 ? climbAcceleration : climbDeceleration;
        velocity.x = Mathf.MoveTowards(velocity.x, desiredSpeedX, accelerationX * Time.deltaTime);

        float desiredSpeedY = useInput ? Input.normalized.y * maxClimbSpeed + addForceY : 0;
        float accelerationY = useInput && inputVertical != 0 ? climbAcceleration : climbDeceleration;
        velocity.y = Mathf.MoveTowards(velocity.y, desiredSpeedY, accelerationY * Time.deltaTime);
    }

    void ChargeClimbJump()
    {
        if (jumpKeyWasPressed && !JumpKeyPressed()) jumpKeyWasPressed = false;

        // Is starting to climb jump
        if (!chargingClimbJump && !jumpKeyWasPressed && JumpKeyPressed())
        {
            climbJumpChargeTime = 0;
            chargingClimbJump = true;
            jumpKeyWasPressed = true;
            ShowClimbJumpIndicator(true);
        }

        if (!chargingClimbJump) return;

        Vector2 currentInputDirection = new Vector2(inputHorizontal / 2, inputVertical / 2).normalized;

        float currentJumpDistance;
        if (climbJumpChargeTime > minJumpChargeTime)
            currentJumpDistance = Mathf.Lerp(minJumpDistance, maxJumpDistance, (climbJumpChargeTime - minJumpChargeTime) / (maxJumpChargeTime - minJumpChargeTime));
        else currentJumpDistance = Mathf.Lerp(0, minJumpDistance, climbJumpChargeTime / minJumpChargeTime);

        if (!JumpKeyPressed()) TryPerformClimbJump(currentInputDirection, currentJumpDistance);
        else
        {
            UpdateClimbJumpIndicator(currentInputDirection, currentJumpDistance);
            climbJumpChargeTime += Time.deltaTime;
        }
    }

    void TryPerformClimbJump(Vector2 currentJumpDirection, float jumpDistance)
    {
        chargingClimbJump = false;
        ShowClimbJumpIndicator(false);

        if (IsClimbJumpInputValid(currentJumpDirection) && climbJumpChargeTime > minJumpChargeTime)
        {
            velocity = (currentJumpDirection * jumpDistance) * (1 / jumpTravelTime);
            StartCoroutine(PerformClimbJump());
        }
    }

    void ShowClimbJumpIndicator(bool show)
    {
        ShowClimbJumpArrow(show);
        climbJumpCircle.SetActive(show);
    }

    void ShowClimbJumpArrow(bool show)
    {
        climbJumpArrow.SetActive(show);
    }

    void UpdateClimbJumpIndicator(Vector2 currentDirection, float lenght)
    {
        if (IsClimbJumpInputValid(currentDirection) && climbJumpChargeTime > minJumpChargeTime)
        {
            float rot = Mathf.Atan2(currentDirection.y, currentDirection.x) * Mathf.Rad2Deg - 90;
            climbJumpArrow.transform.rotation = Quaternion.Euler(0, 0, rot);
            ShowClimbJumpArrow(true);
        }
        else ShowClimbJumpArrow(false);

        climbJumpArrow.transform.localScale = Vector3.one * lenght * (1 - spriteRimThickness);
        climbJumpCircle.transform.localScale = Vector3.one * lenght * 2;
    }

    bool IsClimbJumpInputValid(Vector2 direction)
    {
        return Mathf.Abs(direction.magnitude) > 0.8f;
    }

    bool JumpKeyPressed()
    {
        return input.Player.Jump.ReadValue<float>() > 0;
    }

    void InputJump()
    {
        // ground jump
        if (canMove && isGrounded && !isWaterActive && !isClimbing) Jump();

        // effect surface jump
        if (canMove && !isEffectJumping && (IsOnSurface(activeWindEffect, effectSurfaceTolerance) || IsOnSurface(activeWaterEffect, effectSurfaceTolerance)))
        {
            StartCoroutine(EffectJumpCooldown(0.5f));
            Jump();
        }
    }

    void Jump()
    {
        velocity.y = Mathf.Sqrt(jumpHeight * -2 * -gravity);
    }

    void TryChangeRooms(Room newRoom)
    {
        if (newRoom && newRoom != currentRoom)
        {
            currentRoom = newRoom;
            followCamera.ChangeRooms(newRoom);
            SceneStreamingManager.Instance.SetCurrentlyPlayedScene(newRoom.gameObject.scene.name);
        }
    }

    void InputModuleInteraction(bool started)
    {
        if (currentModule)
        {
            if (started) StartCoroutine(nameof(ModuleInteraction));
            else CancelModuleInteraction();
        }
    }

    void CancelModuleInteraction()
    {
        StopCoroutine(nameof(ModuleInteraction));
        AudioManager.instance.StopSound("ModuleInteraction");
        isInteractingWithModule = false;
    }

    void InputShowEffectPreview(bool show)
    {
        showEffectPreviews = show;
    }

    void InputShowEffectPreviewToggle()
    {
        showEffectPreviews = !showEffectPreviews;
    }

    void UpdateEffectPreview()
    {
        if (!currentRoom) return;
        if (currentModule)
        {
            List<EffectVolumeBase> nonModuleEffects = new List<EffectVolumeBase>();
            currentRoom.GetComponentsInChildren(nonModuleEffects);
            EffectVolumeBase[] moduleEffects = currentModule.ControlledEffects;
            foreach (var ev in moduleEffects)
                if (ev)
                {
                    ev.ShowPreview(true);
                    nonModuleEffects.Remove(ev);
                }

            foreach (var ev in nonModuleEffects) ev.ShowPreview(showEffectPreviews);
        }
        else foreach (var ev in currentRoom.GetComponentsInChildren<EffectVolumeBase>()) ev.ShowPreview(showEffectPreviews);
    }

    void InputToggleClimbing()
    {
        // player could abuse less friction in air, i.e. a longer jump.
        if (!canMove || (isClimbing && chargingClimbJump)) return;
        if (availableClimbables.Count > 0)
        {
            isClimbing = !isClimbing;
            if (isClimbing) playerFX.EnterClimbing();
        }
    }

    float GetCurrentSidewaysWindSpeed()
    {
        if (isWindActive && activeWindDirection == WindDirection.Left) return -windSidewaysSpeed;
        if (isWindActive && activeWindDirection == WindDirection.Right) return windSidewaysSpeed;
        return 0;
    }

    bool IsOnSurface(EffectVolumeBase effect, float tolerance = 0)
    {
        return TryGetTransformSurfacePosY(effect, out float surfacePosY) && surfacePosY - tolerance <= transform.position.y;
    }

    bool TryGetTransformSurfacePosY(EffectVolumeBase effect, out float surfacePosY)
    {
        if (effect)
        {
            surfacePosY = effect.transform.position.y + effect.Height;
            return true;
        }
        surfacePosY = 0;
        return false;
    }

    Collider[] CheckCapsuleSidesCollision()
    {
        Vector3 halfSize = new Vector3(controller.radius + capsuleSidesCheckOffset, controller.height / 2 - controller.radius, controller.radius);
        return Physics.OverlapBox(transform.position, halfSize, Quaternion.identity, groundLayer, QueryTriggerInteraction.Ignore);
    }

    Collider[] CheckCapsuleEndCollision(out bool collided, bool bottomEnd = true)
    {
        float sphereOffset = controller.height / 2 - controller.radius + capsuleEndCheckOffset;
        sphereOffset = bottomEnd ? -sphereOffset : sphereOffset;
        Vector3 sphereCenter = new Vector3(transform.position.x, transform.position.y + sphereOffset, transform.position.z);

        Collider[] colliders = Physics.OverlapSphere(sphereCenter, controller.radius - 0.01f, groundLayer, QueryTriggerInteraction.Ignore);

        collided = colliders.Length > 0;
        return colliders;
    }

    bool CheckIfPlayerCrushed(Collider[] ceilingCollider, Collider[] groundCollider)
    {
        // check if top and bottom collide with any ground
        if (ceilingCollider.Length > 0 && groundCollider.Length > 0)
        {
            Collider[] allCollider = new Collider[ceilingCollider.Length + groundCollider.Length];
            ceilingCollider.CopyTo(allCollider, 0);
            groundCollider.CopyTo(allCollider, ceilingCollider.Length);

            // check if any collider is tagged as CrushPlayer
            foreach (Collider c in allCollider)
                if (c.CompareTag("CrushPlayer")) return true;
        }
        return false;
    }

    void HandleWallCollision(Collider[] sidesCollider)
    {
        if (sidesCollider.Length > 0) velocity.x = 0;
    }

    void HandleMovingPlatformAttachment(Collider[] groundCollider)
    {
        foreach (Collider c in groundCollider)
            if (c.gameObject.layer == LayerMask.NameToLayer("GroundMoving"))
            {
                transform.parent = c.transform;
                return;
            }
        DetachFromPlatform();
    }

    public void DetachFromPlatform()
    {
        if (transform.parent != null)
        {
            transform.parent = null;
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
        }
    }

    public void Teleport(Vector3 position)
    {
        velocity = Vector3.zero;
        transform.position = new Vector3(position.x, position.y, defaultZPosition);
    }

    public void SetCanMove(bool canMove)
    {
        this.canMove = canMove;
    }

    public void SetVelocity(Vector3 velocity)
    {
        this.velocity = velocity;
    }

    public Vector2 GetSwimVelocityNegOneToOne()
    {
        float yVelocity = Mathf.Clamp(velocity.y / maxSwimSpeed, -1, 1);
        yVelocity = Mathf.Abs(yVelocity) > 0.1f ? yVelocity : 0;
        return new Vector2(Mathf.Clamp(velocity.x / maxSwimSpeed, -1, 1), yVelocity);
    }

    void EvaluateActiveEffects()
    {
        bool windActive = false;
        WindDirection windDirectionActive = WindDirection.Up;
        EffectVolumeBase windEffect = null;

        bool waterActive = false;
        EffectVolumeBase waterEffect = null;

        foreach (EffectVolumeBase ev in activeEffects.ToList()) if (!ev.IsActive) activeEffects.Remove(ev);

        foreach (EffectVolumeBase ev in activeEffects.ToList())
        {
            if (ev.ActiveEffect.IsWind())
            {
                windActive = true;
                windDirectionActive = ev.ActiveWindDirection;
                if (windDirectionActive == WindDirection.Up) windEffect = ev;
            }

            if (ev.ActiveEffect.IsWater())
            {
                waterActive = true;
                waterEffect = ev;
            }
        }

        // wind
        if (!isWindActive && windActive && windDirectionActive == WindDirection.Up) StartCoroutine(EffectJumpCooldown(windEnterCooldown));
        isWindActive = windActive;
        activeWindEffect = windEffect;
        activeWindDirection = windDirectionActive;

        // water
        if (!isWaterActive && waterActive)
        {
            StartCoroutine(EffectJumpCooldown(enterWaterCooldown));
            WaterEntered(true);
        }
        if (isWaterActive && !waterActive) WaterEntered(false);

        isWaterActive = waterActive;
        activeWaterEffect = waterEffect;
    }

    void WaterEntered(bool entered)
    {
        if (entered)
        {
            // adjust velocity if too high
            float maxVelocity = maxSwimSpeed * enterWaterMaxSpeedMult;
            velocity.x = Mathf.Clamp(velocity.x, -maxVelocity, maxVelocity);
            velocity.y = Mathf.Clamp(velocity.y, -maxVelocity, maxVelocity);
        }

        // trigger water vfx
        playerFX.SetWaterBubblesActive(entered);
        playerFX.SetWaterSplashesActive(entered);

        if (TryGetTransformSurfacePosY(activeWaterEffect, out float waterSurface))
            playerFX.SpawnWaterBurst(new Vector3(transform.position.x, waterSurface + 0.3f, transform.position.z));
        else playerFX.SpawnWaterBurst(transform.position);
    }

    void OnClimbJumpToClimbable()
    {
        // if still climbing after jump
        // velocity has to be set to zero, or the player decelerate and thus the idicator wont be acurate.
        velocity = Vector3.zero;
        playerFX.EnterClimbing();
    }

    IEnumerator EffectJumpCooldown(float cooldown)
    {
        isEffectJumping = true;
        yield return new WaitForSeconds(cooldown);
        isEffectJumping = false;
    }

    IEnumerator PerformClimbJump()
    {
        performingClimbJump = true;
        yield return new WaitForSeconds(jumpTravelTime);
        performingClimbJump = false;
        if (isClimbing) OnClimbJumpToClimbable();
    }

    IEnumerator ModuleInteraction()
    {
        // start interaction sound
        AudioManager.instance.PlaySound("ModuleInteraction");

        // init interaction
        isInteractingWithModule = true;
        TryGetComponent(out PlayerUI playerUI);
        Module interactingModule = currentModule;
        float timer = 0;

        // update interaction
        while (timer < moduleActivationTime)
        {
            if (!currentModule || interactingModule != currentModule)
            {
                // player left module trigger
                AudioManager.instance.StopSound("ModuleInteraction");
                isInteractingWithModule = false;
                yield break;
            }
            timer += Time.deltaTime;
            if (playerUI) playerUI.interactionProgress = timer / moduleActivationTime;
            yield return null;
        }
        // interaction succesfull
        isInteractingWithModule = false;
        if (currentModule) currentModule.Interact();
    }
}
