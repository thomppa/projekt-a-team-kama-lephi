﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZonesTitan : MonoBehaviour
{
    AudioManager audioManager;

    public GameObject batteries;

    public GameObject eye1;
    public GameObject eye2;

    public GameObject eyeLight;
    public GameObject batterieLight;


    Animator eyeAnimator1;
    Animator eyeAnimator2;

    private void Start()
    {
        audioManager = AudioManager.instance;

        eyeAnimator1 = eye1.GetComponent<Animator>();
        eyeAnimator2 = eye2.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Delay());
        }

        IEnumerator Delay()
        {
            audioManager.StopSound("TutorialLoop");
            yield return new WaitForSeconds(3);
            audioManager.PlaySound("TitanOn");
            batteries.SetActive(true);
            batterieLight.SetActive(true);
            eyeAnimator1.SetBool("EyesActivating", true);
            eyeAnimator2.SetBool("EyesActivating2", true);
            yield return new WaitForSeconds(2);
            eyeLight.SetActive(true);

        }
    }
}
    