﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneTitan2 : MonoBehaviour
{
    public GameObject batteries;

    public GameObject eye3;
    public GameObject eye4;
    public GameObject eye5;

    public GameObject eyeLight;
    public GameObject batterieLight;


    Animator eyeAnimator3;
    Animator eyeAnimator4;
    Animator eyeAnimator5;

    private void Start()
    {
        eyeAnimator3 = eye3.GetComponent<Animator>();
        eyeAnimator4 = eye4.GetComponent<Animator>();
        eyeAnimator5 = eye5.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Delay());
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(3);
        batteries.SetActive(true);
        batterieLight.SetActive(true);
        eyeAnimator3.SetBool("EyesActivating3", true);
        eyeAnimator4.SetBool("EyesActivating4", true);
        eyeAnimator5.SetBool("EyesActivating5", true);
        yield return new WaitForSeconds(2);
        eyeLight.SetActive(true);

    }
}
